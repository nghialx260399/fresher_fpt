﻿using Phone_Accessories.ViewModels.Colors;

namespace Phone_Accessories.Application.Colors
{
    public interface IColorService
    {
        Task<IList<ColorVm>> GetAll();

        Task<bool> Create(ColorVm categoryVm);

        Task<ColorVm> GetById(int id);

        Task<bool> Update(ColorVm categoryVm);

        Task<bool> Delete(int id);

        Task<IList<ColorVm>> Search(string categoryName);
    }
}