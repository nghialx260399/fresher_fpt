﻿using Phone_Accessories.Data.Infastructures;
using Phone_Accessories.Models.Entity;

namespace Phone_Accessories.Data.IRepositories
{
    public interface ICartDetailRepository : IGenericRepository<CartDetail>
    {
        void DeleteVer1(CartDetail cart);
    }
}