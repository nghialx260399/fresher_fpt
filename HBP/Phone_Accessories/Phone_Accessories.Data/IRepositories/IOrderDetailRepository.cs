﻿using Phone_Accessories.Data.Infastructures;
using Phone_Accessories.Models.Entity;
using System.Linq.Expressions;

namespace Phone_Accessories.Data.IRepositories
{
    public interface IOrderDetailRepository : IGenericRepository<OrderDetail>
    {
        Task<IList<OrderDetail>> GetAllInclude(Expression<Func<OrderDetail, bool>> condition);
    }
}