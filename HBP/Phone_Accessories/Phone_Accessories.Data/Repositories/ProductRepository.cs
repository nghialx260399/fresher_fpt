﻿using Phone_Accessories.Data.Infastructures;
using Phone_Accessories.Data.IRepositories;
using Phone_Accessories.Models.Entity;

namespace Phone_Accessories.Data.Repositories
{
    public class ProductRepository : GenericRepository<Product>, IProductRepository
    {
        public ProductRepository(ApplicationDbContext context) : base(context)
        {
        }
    }
}