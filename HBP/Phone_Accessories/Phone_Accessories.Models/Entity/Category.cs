﻿using Phone_Accessories.Models.BaseEntity;

namespace Phone_Accessories.Models.Entity
{
    public class Category : EntityBase
    {
        public string? Name { get; set; }

        public bool Status { get; set; }

        public IEnumerable<Product>? Products { get; set; }
    }
}