﻿using Phone_Accessories.Models.BaseEntity;

namespace Phone_Accessories.Models.Entity
{
    public class Color : EntityBase
    {
        public string? Name { get; set; }

        public IEnumerable<Product>? Products { get; set; }
    }
}