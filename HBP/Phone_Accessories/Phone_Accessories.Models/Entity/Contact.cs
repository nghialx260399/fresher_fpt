﻿using Phone_Accessories.Models.BaseEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phone_Accessories.Models.Entity
{
    public class Contact : EntityBase
    {
        public string? Phone { get; set; }

        public string? NameUser { get; set; }

        public string? Content { get; set; }
    }
}
