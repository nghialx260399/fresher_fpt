﻿using Phone_Accessories.ViewModels.EntityBaseVms;

namespace Phone_Accessories.ViewModels.Products
{
    public class CreateProduct : EntityBaseVm
    {
        public string? Name { get; set; }

        public string? Description { get; set; }

        public byte[]? Image { get; set; }

        public int ColorId { get; set; }

        public int? SizeId { get; set; }

        public int Quantity { get; set; }

        public decimal Price { get; set; }

        public int PromotionPrice { get; set; }

        public int CategoryId { get; set; }
    }
}