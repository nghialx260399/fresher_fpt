﻿using JustBlog.Application.Comments;
using JustBlog.Models.Entity;
using JustBlog.ViewModels.Comments;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JustBlog.Api.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = "Bearer")]
    public class CommentController : ControllerBase
    {
        private readonly ICommentService commentService;

        public CommentController(ICommentService commentService)
        {
            this.commentService = commentService;
        }

        [HttpGet]
        public IActionResult GetAll([FromQuery] string keyword = null, int pageIndex = 1, int pageSize = 5)
        {
            Func<Comment, bool> filter = null;
            if (!string.IsNullOrEmpty(keyword))
            {
                keyword = keyword.ToLower();
                filter = x => x.Name.ToLower().Contains(keyword);
            }
            var commentPaged = this.commentService.GetPaging(filter: filter, isDeleted: true, pageIndex: pageIndex, pageSize: pageSize);

            return Ok(commentPaged.Data);
        }

        [HttpGet("{id}")]
        public IActionResult GetById(int id)
        {
            var commentExists = commentService.GetById(id);

            if (commentExists != null)
                return Ok(commentExists);

            return NotFound(id);
        }

        [HttpPost]
        public IActionResult Post(CommentVm commentVm)
        {
            if (!ModelState.IsValid)
                return BadRequest("Invalid Data");

            bool isSuccess = commentService.Create(commentVm);

            if (isSuccess)
                return Ok(isSuccess);

            return BadRequest();
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            bool isSuccess = commentService.Delete(id);

            if (isSuccess)
                return Ok(isSuccess);

            return NotFound(id);
        }
    }
}
