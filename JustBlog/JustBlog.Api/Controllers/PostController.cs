﻿using JustBlog.Application.Posts;
using JustBlog.Models.Entity;
using JustBlog.ViewModels.Api;
using JustBlog.ViewModels.Posts;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;

namespace JustBlog.Api.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = "Bearer")]
    public class PostController : ControllerBase
    {
        private readonly IPostService postService;

        public PostController(IPostService postService)
        {
            this.postService = postService;
        }

        //  Get: api/post
        [HttpGet]
        public IActionResult GetAll([FromQuery] PostFillter postFillter)
        {
            Func<Post, bool> filter = null;
            if (!string.IsNullOrEmpty(postFillter.Keyword))
            {
                postFillter.Keyword = postFillter.Keyword.ToLower();
                filter = x => x.Title.ToLower().Contains(postFillter.Keyword);
            }
            var postPaged = this.postService.GetPaging(filter: filter, isDeleted: true, pageIndex: postFillter.PageIndex, pageSize: postFillter.PageSize);

            return Ok(postPaged);
        }

        [HttpGet("{id}")]
        [Authorize(Roles = "Blog Owner")]
        public IActionResult GetById(int id)
        {
            var postExists = postService.GetById(id);

            if (postExists != null)
                return Ok(postExists);

            return NotFound(id);
        }

        [HttpPost]
        public IActionResult Post(PostVm postVm)
        {
            if (!ModelState.IsValid)
                return BadRequest("Invalid Data");

            bool isSuccess = postService.Create(postVm);

            if (isSuccess)
                return Ok(isSuccess);

            return BadRequest();
        }

        [HttpPut]
        public IActionResult Put(PostVm postVm)
        {
            if (!ModelState.IsValid)
                return BadRequest("Invalid Data");

            bool isSuccess = postService.Update(postVm);

            if (isSuccess)
                return Ok(isSuccess);

            return BadRequest();
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            bool isSuccess = postService.Delete(id);

            if (isSuccess)
                return Ok(isSuccess);

            return NotFound(id);
        }
    }
}