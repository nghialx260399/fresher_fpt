﻿using JustBlog.Application.Tags;
using JustBlog.Models.Entity;
using JustBlog.ViewModels.Tags;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JustBlog.Api.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = "Bearer")]
    public class TagController : ControllerBase
    {
        private readonly ITagService tagService;

        public TagController(ITagService tagService)
        {
            this.tagService = tagService;
        }

        [HttpGet]
        public IActionResult GetAll([FromQuery] string keyword = null, int pageIndex = 1, int pageSize = 5)
        {
            Func<Tag, bool> filter = null;
            if (!string.IsNullOrEmpty(keyword))
            {
                keyword = keyword.ToLower();
                filter = x => x.Name.ToLower().Contains(keyword);
            }
            var tagPaged = this.tagService.GetPaging(filter: filter, isDeleted: true, pageIndex: pageIndex, pageSize: pageSize);

            return Ok(tagPaged.Data);
        }

        [HttpGet("{id}")]
        public IActionResult GetById(int id)
        {
            var tagExists = tagService.GetById(id);

            if (tagExists != null)
                return Ok(tagExists);

            return NotFound(id);
        }

        [HttpPost]
        public IActionResult Post(TagVm tagVm)
        {
            if (!ModelState.IsValid)
                return BadRequest("Invalid Data");

            bool isSuccess = tagService.Create(tagVm);

            if (isSuccess)
                return Ok(isSuccess);

            return BadRequest();
        }

        [HttpPut]
        public IActionResult Put(TagVm tagVm)
        {
            if (!ModelState.IsValid)
                return BadRequest("Invalid Data");

            bool isSuccess = tagService.Update(tagVm);

            if (isSuccess)
                return Ok(isSuccess);

            return BadRequest();
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            bool isSuccess = tagService.Delete(id);

            if (isSuccess)
                return Ok(isSuccess);

            return NotFound(id);
        }
    }
}
