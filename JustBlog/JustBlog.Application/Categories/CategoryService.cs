﻿using JustBlog.Data.Infastructures;
using JustBlog.Models.Entity;
using JustBlog.ViewModels.Categories;
using System;
using System.Collections.Generic;

namespace JustBlog.Application.Categories
{
    public class CategoryService : ICategoryService
    {
        private readonly IUnitOfWork unitOfWork;

        public CategoryService(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public bool Create(CategoryVm categoryVm)
        {
            try
            {
                var category = new Category()
                {
                    Name = categoryVm.Name,
                    UrlStug = categoryVm.UrlStug,
                    Description = categoryVm.Description,
                };

                this.unitOfWork.CategoryRepository.Add(category);
                this.unitOfWork.SaveChanges();

                return true;
            }
            catch (Exception)
            {
                throw new Exception("Can't Create a new Category");
            }
        }

        public bool Delete(int id)
        {
            try
            {
                unitOfWork.CategoryRepository.Delete(false, id);
                unitOfWork.SaveChanges();

                return true;
            }
            catch (Exception)
            {
                throw new Exception("Can't Delete a new Category");
            }
        }

        public IList<CategoryVm> GetAll(bool isDeleted = false)
        {
            var categories = this.unitOfWork.CategoryRepository.GetAll(true);
            var cagoryVms = new List<CategoryVm>();

            foreach (var category in categories)
            {
                var cateVm = new CategoryVm()
                {
                    Id = category.Id,
                    Name = category.Name,
                    IsDeleted = category.IsDeleted,
                    UrlStug = category.UrlStug,
                    Description = category.Description,
                    CreatedOn = category.CreatedOn,
                    UpdatedOn = category.UpdatedOn
                };
                cagoryVms.Add(cateVm);
            }
            return cagoryVms;
        }

        public CategoryVm GetById(int id)
        {
            var category = unitOfWork.CategoryRepository.GetById(id);

            var categoryVm = new CategoryVm();

            categoryVm.Id = category.Id;
            categoryVm.Name = category.Name;
            categoryVm.IsDeleted = category.IsDeleted;
            categoryVm.UrlStug = category.UrlStug;
            categoryVm.Description = category.Description;
            categoryVm.CreatedOn = category.CreatedOn;
            categoryVm.UpdatedOn = category.UpdatedOn;

            return categoryVm;
        }

        public bool Update(CategoryVm categoryVm)
        {
            try
            {
                var category = new Category()
                {
                    Id = categoryVm.Id,
                    Name = categoryVm.Name,
                    IsDeleted = categoryVm.IsDeleted,
                    UrlStug = categoryVm.UrlStug,
                    Description = categoryVm.Description,
                    CreatedOn = categoryVm.CreatedOn,
                    UpdatedOn = categoryVm.UpdatedOn,
                };

                this.unitOfWork.CategoryRepository.Update(category);
                this.unitOfWork.SaveChanges();

                return true;
            }
            catch (Exception)
            {
                throw new Exception("Can't Update a new Category");
            }
        }
    }
}