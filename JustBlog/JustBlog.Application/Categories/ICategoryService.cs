﻿using JustBlog.ViewModels.Categories;
using System.Collections.Generic;

namespace JustBlog.Application.Categories
{
    public interface ICategoryService
    {
        IList<CategoryVm> GetAll(bool isDeleted = false);

        bool Create(CategoryVm categoryVm);

        CategoryVm GetById(int id);

        bool Update(CategoryVm categoryVm);

        bool Delete(int id);
    }
}