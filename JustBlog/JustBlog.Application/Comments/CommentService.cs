﻿using JustBlog.Data.Infastructures;
using JustBlog.Models.Entity;
using JustBlog.ViewModels.BaseEntityVm;
using JustBlog.ViewModels.Comments;
using System;
using System.Collections.Generic;
using System.Linq;

namespace JustBlog.Application.Comments
{
    public class CommentService : ICommentService
    {
        private readonly IUnitOfWork unitOfWork;

        public CommentService(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public bool Create(CommentVm commentVm)
        {
            try
            {
                var commnet = new Comment()
                {
                    Name = commentVm.Name,
                    Email = commentVm.Email,
                    CommentHeader = commentVm.CommentHeader,
                    CommentText = commentVm.CommentText,
                    CommentTime = DateTime.Now,
                    PostId = commentVm.PostId
                };

                this.unitOfWork.CommentRepository.Add(commnet);
                this.unitOfWork.SaveChanges();

                return true;
            }
            catch (Exception)
            {
                throw new Exception();
            }
        }

        public bool Delete(int id)
        {
            try
            {
                this.unitOfWork.CommentRepository.Delete(false, id);
                this.unitOfWork.SaveChanges();

                return true;
            }
            catch (Exception)
            {
                throw new Exception();
            }
        }

        public IList<CommentVm> GetAll(bool isDeleted = false)
        {
            var comments = this.unitOfWork.CommentRepository.GetAll();
            var commentVms = new List<CommentVm>();

            foreach (var item in comments)
            {
                var commentVm = new CommentVm()
                {
                    Id = item.Id,
                    Name = item.Name,
                    Email = item.Email,
                    CommentHeader = item.CommentHeader,
                    CommentText = item.CommentText,
                    CommentTime = item.CommentTime,
                    PostId = item.PostId,
                    CreatedOn = item.CreatedOn,
                    UpdatedOn = item.UpdatedOn,
                    IsDeleted = item.IsDeleted
                };

                commentVms.Add(commentVm);
            }

            return commentVms;
        }

        public CommentVm GetById(int id)
        {
            var comment = this.unitOfWork.CommentRepository.GetById(id);

            if (comment != null)
            {
                var commentVm = new CommentVm()
                {
                    Id = comment.Id,
                    Name = comment.Name,
                    Email = comment.Email,
                    CommentHeader = comment.CommentHeader,
                    CommentText = comment.CommentText,
                    CommentTime = comment.CommentTime,
                    PostId = comment.PostId,
                    CreatedOn = comment.CreatedOn,
                    UpdatedOn = comment.UpdatedOn,
                    IsDeleted = comment.IsDeleted
                };

                return commentVm;
            }

            throw new Exception();
        }

        public IList<CommentVm> GetByPost(int postId)
        {
            var comments = this.unitOfWork.CommentRepository.Find(x=>x.PostId == postId);
            var commentVms = new List<CommentVm>();

            foreach (var item in comments)
            {
                var commentVm = new CommentVm()
                {
                    Id = item.Id,
                    Name = item.Name,
                    Email = item.Email,
                    CommentHeader = item.CommentHeader,
                    CommentText = item.CommentText,
                    CommentTime = item.CommentTime,
                    PostId = item.PostId,
                    CreatedOn = item.CreatedOn,
                    UpdatedOn = item.UpdatedOn,
                    IsDeleted = item.IsDeleted
                };

                commentVms.Add(commentVm);
            }

            return commentVms;
        }

        public PagedVm<CommentVm> GetPaging(Func<Comment, bool> filter, bool isDeleted = false, int pageSize = 10, int pageIndex = 1)
        {
            Func<IEnumerable<Comment>, IOrderedEnumerable<Comment>> order = x => x.OrderBy(x => x.PostId);
            var comments = this.unitOfWork.CommentRepository
                      .GetPaging(filter: filter, orderBy: order, isDeleted: isDeleted, pageSize: pageSize, pageIndex: pageIndex);

            List<CommentVm> commentVms = new List<CommentVm>();
            foreach (var item in comments.Data)
            {
                var commentVm = new CommentVm()
                {
                    Id = item.Id,
                    Name = item.Name,
                    Email = item.Email,
                    CommentHeader = item.CommentHeader,
                    CommentText = item.CommentText,
                    CommentTime = item.CommentTime,
                    PostId = item.PostId,
                    CreatedOn = item.CreatedOn,
                    UpdatedOn = item.UpdatedOn,
                    IsDeleted = item.IsDeleted
                };
                commentVms.Add(commentVm);
            }
            return new PagedVm<CommentVm>(commentVms, pageIndex, pageSize, comments.TotalPage);
        }

        public bool Update(CommentVm commentVm)
        {
            try
            {
                var comment = new Comment()
                {
                    Id = commentVm.Id,
                    Name = commentVm.Name,
                    Email = commentVm.Email,
                    CommentHeader = commentVm.CommentHeader,
                    CommentText = commentVm.CommentText,
                    CommentTime = commentVm.CommentTime,
                    PostId = commentVm.PostId,
                    CreatedOn = commentVm.CreatedOn,
                    UpdatedOn = commentVm.UpdatedOn,
                    IsDeleted = commentVm.IsDeleted
                };

                this.unitOfWork.CommentRepository.Update(comment);
                this.unitOfWork.SaveChanges();

                return true;
            }
            catch (Exception)
            {
                throw new Exception();
            }
        }
    }
}