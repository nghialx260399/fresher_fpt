﻿using JustBlog.Models.Entity;
using JustBlog.ViewModels.BaseEntityVm;
using JustBlog.ViewModels.Comments;
using System;
using System.Collections.Generic;

namespace JustBlog.Application.Comments
{
    public interface ICommentService
    {
        IList<CommentVm> GetAll(bool isDeleted = false);

        bool Create(CommentVm commentVm);

        CommentVm GetById(int id);

        bool Update(CommentVm commentVm);

        bool Delete(int id);

        IList<CommentVm> GetByPost(int postId);

        PagedVm<CommentVm> GetPaging(Func<Comment, bool> filter, bool isDeleted = false, int pageSize = 10, int pageIndex = 1);
    }
}