﻿using JustBlog.Models.Entity;
using JustBlog.ViewModels.BaseEntityVm;
using JustBlog.ViewModels.Posts;
using System;
using System.Collections.Generic;

namespace JustBlog.Application.Posts
{
    public interface IPostService
    {
        IList<PostVm> GetAll(bool isDeleted = false);

        IList<PostVm> GetPostsByCategory(int categoryId, bool isDeleted = false);

        bool Create(PostVm postVm);

        PostVm GetById(int id);

        bool Update(PostVm postVm);

        bool Delete(int id);

        PagedVm<PostVm> GetPaging(Func<Post, bool> filter, bool isDeleted = false, int pageSize = 10, int pageIndex = 1);


        IList<PostVm> GetPostsByTag(string tagName, bool isDeleted = false);

        bool Publish(int postId);

        bool UnPublish(int postId);
    }
}