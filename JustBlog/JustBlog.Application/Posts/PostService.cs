﻿using AutoMapper;
using JustBlog.Data.Infastructures;
using JustBlog.Models.Entity;
using JustBlog.ViewModels.BaseEntityVm;
using JustBlog.ViewModels.Posts;
using System;
using System.Collections.Generic;
using System.Linq;

namespace JustBlog.Application.Posts
{
    public class PostService : IPostService
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly IMapper mapper;

        public PostService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
        }

        public bool Create(PostVm postVm)
        {
            try
            {
                var postTagMaps = new List<PostTagMap>();

                if (!string.IsNullOrEmpty(postVm.Tags))
                {
                    var tagIds = this.unitOfWork.TagRepository.AddTagByString(postVm.Tags);

                    foreach (var tagId in tagIds)
                    {
                        var postTagMap = new PostTagMap()
                        {
                            TagId = tagId
                        };
                        postTagMaps.Add(postTagMap);
                    }
                }

                var post = new Post()
                {
                    Title = postVm.Title,
                    ShortDescription = postVm.ShortDescription,
                    PostContent = postVm.PostContent,
                    UrlSlug = postVm.UrlSlug,
                    Published = postVm.Published,
                    PostedOn = postVm.PostedOn,
                    Modified = postVm.Modified,
                    CategoryId = postVm.CategoryId,
                    ViewCount = postVm.ViewCount,
                    RateCount = 1,
                    TotalRate = postVm.TotalRate,
                    Rate = postVm.Rate,
                    PostTagMaps = postTagMaps
                };

                unitOfWork.PostRepository.Add(post);
                unitOfWork.SaveChanges();

                return true;
            }
            catch (Exception)
            {
                throw new Exception("Create fail");
            }
        }

        public bool Delete(int id)
        {
            try
            {
                unitOfWork.PostRepository.Delete(false, id);
                unitOfWork.SaveChanges();

                return true;
            }
            catch (Exception)
            {
                throw new Exception("Can't Delete a new Category");
            }
        }

        public IList<PostVm> GetAll(bool isDeleted = false)
        {
            var posts = this.unitOfWork.PostRepository.GetAll(isDeleted);
            var postVms = new List<PostVm>();

            foreach (var post in posts)
            {
                var postVm = new PostVm()
                {
                    Id = post.Id,
                    Title = post.Title,
                    ShortDescription = post.ShortDescription,
                    PostContent = post.PostContent,
                    UrlSlug = post.UrlSlug,
                    Published = post.Published,
                    PostedOn = post.PostedOn,
                    Modified = post.Modified,
                    CategoryId = post.CategoryId,
                    ViewCount = post.ViewCount,
                    RateCount = post.RateCount,
                    TotalRate = post.TotalRate,
                    Rate = post.Rate,
                    CreatedOn = post.CreatedOn,
                    UpdatedOn = post.UpdatedOn,
                    IsDeleted = post.IsDeleted,
                    Tags = this.unitOfWork.TagRepository.GetTagByPost(post.Id)
                };
                postVms.Add(postVm);
            }
            return postVms;
        }

        public PostVm GetById(int id)
        {
            var post = unitOfWork.PostRepository.GetById(id);

            if (post != null)
            {
                var postVm = new PostVm()
                {
                    Id = post.Id,
                    Title = post.Title,
                    ShortDescription = post.ShortDescription,
                    PostContent = post.PostContent,
                    UrlSlug = post.UrlSlug,
                    Published = post.Published,
                    PostedOn = post.PostedOn,
                    Modified = post.Modified,
                    CategoryId = post.CategoryId,
                    ViewCount = post.ViewCount,
                    RateCount = post.RateCount,
                    TotalRate = post.TotalRate,
                    Rate = post.Rate,
                    CreatedOn = post.CreatedOn,
                    UpdatedOn = post.UpdatedOn,
                    IsDeleted = post.IsDeleted,
                    Tags = this.unitOfWork.TagRepository.GetTagByPost(id)
                };

                return postVm;
            }
            throw new Exception("Not found");
        }

        public PagedVm<PostVm> GetPaging(Func<Post, bool> filter, bool isDeleted = false, int pageSize = 10, int pageIndex = 1)
        {
            Func<IEnumerable<Post>, IOrderedEnumerable<Post>> order = x => x.OrderBy(x => x.Title);
            var posts = this.unitOfWork.PostRepository
                      .GetPaging(filter: filter, orderBy: order, isDeleted: isDeleted, pageSize: pageSize, pageIndex: pageIndex);

            var postVms = this.mapper.Map<IEnumerable<PostVm>>(posts.Data);

            return new PagedVm<PostVm>(postVms, pageIndex, pageSize, posts.TotalPage);
        }

        public IList<PostVm> GetPostsByCategory(int categoryId, bool isDeleted = false)
        {
            var posts = this.unitOfWork.PostRepository.GetPostsByCategory(categoryId);
            var postVms = new List<PostVm>();

            foreach (var post in posts)
            {
                var postVm = new PostVm()
                {
                    Id = post.Id,
                    Title = post.Title,
                    ShortDescription = post.ShortDescription,
                    PostContent = post.PostContent,
                    UrlSlug = post.UrlSlug,
                    Published = post.Published,
                    PostedOn = post.PostedOn,
                    Modified = post.Modified,
                    CategoryId = post.CategoryId,
                    ViewCount = post.ViewCount,
                    RateCount = post.RateCount,
                    TotalRate = post.TotalRate,
                    Rate = post.Rate,
                    CreatedOn = post.CreatedOn,
                    UpdatedOn = post.UpdatedOn,
                    IsDeleted = post.IsDeleted
                };
                postVms.Add(postVm);
            }
            return postVms;
        }

        public IList<PostVm> GetPostsByTag(string tagName, bool isDeleted = false)
        {
            var posts = this.unitOfWork.PostRepository.GetPostByTagName(tagName);
            var postVms = new List<PostVm>();

            foreach (var post in posts)
            {
                var postVm = new PostVm()
                {
                    Id = post.Id,
                    Title = post.Title,
                    ShortDescription = post.ShortDescription,
                    PostContent = post.PostContent,
                    UrlSlug = post.UrlSlug,
                    Published = post.Published,
                    PostedOn = post.PostedOn,
                    Modified = post.Modified,
                    CategoryId = post.CategoryId,
                    ViewCount = post.ViewCount,
                    RateCount = post.RateCount,
                    TotalRate = post.TotalRate,
                    Rate = post.Rate,
                    CreatedOn = post.CreatedOn,
                    UpdatedOn = post.UpdatedOn,
                    IsDeleted = post.IsDeleted
                };
                postVms.Add(postVm);
            }
            return postVms;
        }

        public bool Publish(int postId)
        {
            var post = this.unitOfWork.PostRepository.GetById(postId);

            if (post != null)
            {
                post.IsDeleted = false;
                post.PostedOn = DateTime.Now;

                this.unitOfWork.PostRepository.Update(post);
                return this.unitOfWork.SaveChanges() > 0;
            }

            return false;

        }

        public bool UnPublish(int postId)
        {
            var post = this.unitOfWork.PostRepository.GetById(postId);

            if (post != null)
            {
                post.IsDeleted = true;
             
                this.unitOfWork.PostRepository.Update(post);
                return this.unitOfWork.SaveChanges() > 0;
            }

            return false;
        }

        public bool Update(PostVm postVm)
        {
            try
            {
                var postTags = this.unitOfWork.ApplicationDbContext
                                    .PostTagMaps.Where(pt => pt.PostId == postVm.Id);
                this.unitOfWork.ApplicationDbContext.PostTagMaps.RemoveRange(postTags);
                this.unitOfWork.SaveChanges();

                var post = this.unitOfWork.PostRepository.GetById(postVm.Id);

                var postTagMaps = new List<PostTagMap>();
                var tagIds = this.unitOfWork.TagRepository.AddTagByString(postVm.Tags);
                foreach (var tagId in tagIds)
                {
                    var postTagMap = new PostTagMap()
                    {
                        TagId = tagId
                    };
                    postTagMaps.Add(postTagMap);
                }

                post.Title = postVm.Title;
                post.ShortDescription = postVm.ShortDescription;
                post.PostContent = postVm.PostContent;
                post.UrlSlug = postVm.UrlSlug;
                post.ViewCount = postVm.ViewCount;
                post.CategoryId = postVm.CategoryId;
                post.IsDeleted = postVm.IsDeleted;
                post.PostTagMaps = postTagMaps;

                unitOfWork.PostRepository.Update(post);
                return unitOfWork.SaveChanges() > 0;
            }
            catch (Exception)
            {
                throw new Exception();
            }
        }
    }
}