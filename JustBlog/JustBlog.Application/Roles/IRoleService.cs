﻿using JustBlog.Models.Entity;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace JustBlog.Application.Roles
{
    public interface IRoleService
    {
        Task<IEnumerable<ApplicationRole>> GetAll();
    }
}