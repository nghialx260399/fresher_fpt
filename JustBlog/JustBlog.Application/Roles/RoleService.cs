﻿using JustBlog.Models.Entity;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace JustBlog.Application.Roles
{
    public class RoleService : IRoleService
    {
        private readonly RoleManager<ApplicationRole> roleManager;

        public RoleService(RoleManager<ApplicationRole> roleManager)
        {
            this.roleManager = roleManager;
        }
        public async Task<IEnumerable<ApplicationRole>> GetAll()
        {
            return await this.roleManager.Roles.ToListAsync();
        }
    }
}