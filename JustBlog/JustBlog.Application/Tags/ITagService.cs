﻿using JustBlog.Models.Entity;
using JustBlog.ViewModels.BaseEntityVm;
using JustBlog.ViewModels.Tags;
using System;
using System.Collections.Generic;

namespace JustBlog.Application.Tags
{
    public interface ITagService
    {
        IList<TagVm> GetAll(bool isDeleted = false);

        bool Create(TagVm tagVm);

        TagVm GetById(int id);

        bool Update(TagVm tagVm);

        bool Delete(int id);

        PagedVm<TagVm> GetPaging(Func<Tag, bool> filter, bool isDeleted = false, int pageSize = 10, int pageIndex = 1);

        string GetTagByPost(int postId, bool isDeleted = false);

        bool UpdateTagByTagName(string tagName);
    }
}