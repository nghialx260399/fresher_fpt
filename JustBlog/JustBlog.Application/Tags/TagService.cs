﻿using JustBlog.Data.Helper;
using JustBlog.Data.Infastructures;
using JustBlog.Models.Entity;
using JustBlog.ViewModels.BaseEntityVm;
using JustBlog.ViewModels.Tags;
using System;
using System.Collections.Generic;
using System.Linq;

namespace JustBlog.Application.Tags
{
    public class TagService : ITagService
    {
        private readonly IUnitOfWork unitOfWork;

        public TagService(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public bool Create(TagVm tagVm)
        {
            try
            {
                var tag = new Tag()
                {
                    Name = tagVm.Name,
                    UrlSlug = tagVm.UrlSlug,
                    Description = tagVm.Description,
                    Count = tagVm.Count
                };

                unitOfWork.TagRepository.Add(tag);
                unitOfWork.SaveChanges();

                return true;
            }
            catch (Exception)
            {
                throw new Exception();
            }
        }

        public bool Delete(int id)
        {
            try
            {
                unitOfWork.TagRepository.Delete(false, id);
                unitOfWork.SaveChanges();

                return true;
            }
            catch (Exception)
            {
                throw new Exception("Can't Delete a new Category");
            }
        }

        public IList<TagVm> GetAll(bool isDeleted = false)
        {
            var tags = this.unitOfWork.TagRepository.GetAll(isDeleted);
            var tagVms = new List<TagVm>();

            foreach (var tag in tags)
            {
                var tagVm = new TagVm()
                {
                    Id = tag.Id,
                    Name = tag.Name,
                    UrlSlug = tag.UrlSlug,
                    Description = tag.Description,
                    Count = tag.Count,
                    CreatedOn = tag.CreatedOn,
                    UpdatedOn = tag.UpdatedOn,
                    IsDeleted = tag.IsDeleted
                };
                tagVms.Add(tagVm);
            }
            return tagVms;
        }

        public TagVm GetById(int id)
        {
            var tag = unitOfWork.TagRepository.GetById(id);

            if (tag != null)
            {
                var tagVm = new TagVm()
                {
                    Id = tag.Id,
                    Name = tag.Name,
                    UrlSlug = tag.UrlSlug,
                    Description = tag.Description,
                    Count = tag.Count,
                    CreatedOn = tag.CreatedOn,
                    UpdatedOn = tag.UpdatedOn,
                    IsDeleted = tag.IsDeleted
                };

                return tagVm;
            }
            throw new Exception("Not found");
        }

        public PagedVm<TagVm> GetPaging(Func<Tag, bool> filter, bool isDeleted = false, int pageSize = 10, int pageIndex = 1)
        {
            Func<IEnumerable<Tag>, IOrderedEnumerable<Tag>> order = x => x.OrderBy(x => x.Name);
            var tags = this.unitOfWork.TagRepository
                      .GetPaging(filter: filter, orderBy: order, isDeleted: isDeleted, pageSize: pageSize, pageIndex: pageIndex);

            List<TagVm> tagVms = new();
            foreach (var tag in tags.Data)
            {
                var tagVm = new TagVm()
                {
                    Id = tag.Id,
                    Name = tag.Name,
                    UrlSlug = tag.UrlSlug,
                    Description = tag.Description,
                    Count = tag.Count,
                    IsDeleted = tag.IsDeleted,
                    CreatedOn = tag.CreatedOn,
                    UpdatedOn = tag.UpdatedOn,
                };
                tagVms.Add(tagVm);
            }
            return new PagedVm<TagVm>(tagVms, pageIndex, pageSize, tags.TotalPage);
        }

        public string GetTagByPost(int postId, bool isDeleted = false)
        {
            return unitOfWork.TagRepository.GetTagByPost(postId);
        }

        public bool Update(TagVm tagVm)
        {
            try
            {
                var tag = new Tag()
                {
                    Id = tagVm.Id,
                    Name = tagVm.Name,
                    UrlSlug = UrlHelper.FrientlyUrl(tagVm.Name),
                    Description = tagVm.Description,
                    Count = tagVm.Count,
                    CreatedOn = tagVm.CreatedOn,
                    UpdatedOn = tagVm.UpdatedOn,
                    IsDeleted = tagVm.IsDeleted
                };

                this.unitOfWork.TagRepository.Update(tag);
                this.unitOfWork.SaveChanges();

                return true;
            }
            catch (Exception)
            {
                throw new Exception("Can't Update a new Category");
            }
        }

        public bool UpdateTagByTagName(string tagName)
        {
            var tagExis = this.unitOfWork.TagRepository.Find(x => x.Name.ToLower().Trim().Equals(tagName.ToLower().Trim())).FirstOrDefault();

            if (tagExis == null)
                return false;

            tagExis.Count++;

            this.unitOfWork.TagRepository.Update(tagExis);
            return this.unitOfWork.SaveChanges() > 0;
        }
    }
}