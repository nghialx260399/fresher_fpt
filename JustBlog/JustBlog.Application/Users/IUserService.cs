﻿using JustBlog.ViewModels.IdentityResult;
using JustBlog.ViewModels.UserRoles;
using System.Threading.Tasks;

namespace JustBlog.Application.Users
{
    public interface IUserService
    {
        Task<IdentityCustomResult> CreateAsync(CreateUserVm request);

        Task<IdentityCustomResult> SignInAsync(LoginVm request);

        Task<IdentityCustomResult> RoleAssignAsync(string email, string roleName);

        Task<IdentityCustomResult> GenerateToken(LoginVm request);
    }
}