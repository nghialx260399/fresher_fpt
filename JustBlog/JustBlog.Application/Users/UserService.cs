﻿using JustBlog.ViewModels.IdentityResult;
using JustBlog.ViewModels.UserRoles;
using JustBlog.WebApp.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace JustBlog.Application.Users
{
    public class UserService : IUserService
    {
        private readonly UserManager<ApplicationUser> userManager;
        private readonly SignInManager<ApplicationUser> signInManager;
        private readonly IConfiguration configuration;

        public UserService(UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signInManager, IConfiguration configuration)
        {
            this.userManager = userManager;
            this.signInManager = signInManager;
            this.configuration = configuration;
        }

        public async Task<IdentityCustomResult> CreateAsync(CreateUserVm request)
        {
            try
            {
                var user = new ApplicationUser()
                {
                    UserName = request.UserName,
                    Email = request.Email,
                };
                var result = await this.userManager.CreateAsync(user, request.Password);

                if (result.Succeeded)
                    return new SuccessResult();

                return new ErrorResult(result.Errors);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public async Task<IdentityCustomResult> GenerateToken(LoginVm request)
        {
            var result = await this.SignInAsync(request);

            if (result.IsSuccessed)
            {
                var user = await this.userManager.FindByEmailAsync(request.Email);
                var roles = await this.userManager.GetRolesAsync(user);

                var claims = new List<Claim>()
                {
                 new Claim(ClaimTypes.Email,user.Email),
                 new Claim(ClaimTypes.NameIdentifier,user.Id.ToString()),
                 new Claim(ClaimTypes.Name,user.UserName),
                //new Claim(ClaimTypes.Role,roles.foreach),
                 new Claim("FullName",user.FirstName),
                 };

                foreach (var item in roles)
                {
                    claims.Add(new Claim(ClaimTypes.Role, item));
                }

                var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(this.configuration["Jwt:Key"]));
                var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256); //rsA

                var token = new JwtSecurityToken(
                   issuer: this.configuration["Jwt:Issuer"],
                   audience: this.configuration["Jwt:Audience"],
                   claims: claims,
                    expires: DateTime.Now.AddHours(1),
                    signingCredentials: creds);

                return new SuccessResult(new JwtSecurityTokenHandler().WriteToken(token));
            }
            return new ErrorResult("Login failed");
        }

        public async Task<IdentityCustomResult> RoleAssignAsync(string email, string roleName)
        {
            try
            {
                var user = await this.userManager.FindByEmailAsync(email);

                if (user == null)
                    return new ErrorResult("Khong tim thay email");

                var result = await this.userManager.AddToRoleAsync(user, roleName);

                if (result.Succeeded)
                    return new SuccessResult();

                return new ErrorResult("Role Assign failed");
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<IdentityCustomResult> SignInAsync(LoginVm request)
        {
            try
            {
                //hash
                var user = await this.userManager.FindByEmailAsync(request.Email);

                if (user == null)
                    return new ErrorResult("Khong tim thay tai khoan");

                var result = await this.signInManager.PasswordSignInAsync(user, request.Password, true, true);

                if (result.Succeeded)
                    return new SuccessResult();

                return new ErrorResult("Dang nhap khong thanh cong");
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}