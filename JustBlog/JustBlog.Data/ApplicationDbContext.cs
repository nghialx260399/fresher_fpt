﻿using JustBlog.Models.BaseEntity;
using JustBlog.Models.Entity;
using JustBlog.WebApp.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;

namespace JustBlog.WebApp.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext(DbContextOptions options)
            : base(options)
        {
        }

        public DbSet<Category> Categories { get; set; }

        public DbSet<Post> Posts { get; set; }

        public DbSet<PostTagMap> PostTagMaps { get; set; }

        public DbSet<Tag> Tags { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Category>()
                        .HasMany<Post>(c => c.Posts)
                        .WithOne(p => p.Category)
                        .HasForeignKey(p => p.CategoryId)
                        .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<Post>()
                       .HasMany<Comment>(p => p.Comments)
                       .WithOne(c => c.Post)
                       .HasForeignKey(c => c.PostId)
                       .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<PostTagMap>().HasKey(pt => new { pt.PostId, pt.TagId });

            base.OnModelCreating(modelBuilder);
        }

        public override int SaveChanges()
        {
            BeforSaveChanges();
            return base.SaveChanges();
        }

        private void BeforSaveChanges()
        {
            var entities = ChangeTracker.Entries();
            foreach (var entity in entities)
            {
                var now = DateTime.Now;
                if (entity.Entity is IEntityBase asEntity)
                {
                    if (entity.State == EntityState.Added)
                    {
                        asEntity.CreatedOn = now;
                        asEntity.UpdatedOn = now;
                    }
                    if (entity.State == EntityState.Modified)
                    {
                        asEntity.UpdatedOn = now;
                    }
                }
            }
        }
    }
}