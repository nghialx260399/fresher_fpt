﻿using JustBlog.Data.Infastructures;
using JustBlog.Models.Entity;

namespace JustBlog.Data.IRepositories
{
    public interface ICategoryRepository : IGenericRepository<Category>
    {
    }
}