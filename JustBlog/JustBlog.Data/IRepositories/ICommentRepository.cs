﻿using JustBlog.Data.Infastructures;
using JustBlog.Models.Entity;

namespace JustBlog.Data.IRepositories
{
    public interface ICommentRepository : IGenericRepository<Comment>
    {
    }
}