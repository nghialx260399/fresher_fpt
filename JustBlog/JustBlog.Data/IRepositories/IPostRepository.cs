﻿using JustBlog.Data.Infastructures;
using JustBlog.Models.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JustBlog.Data.IRepositories
{
    public interface IPostRepository: IGenericRepository<Post>
    {
        IList<Post> GetPostsByCategory(int id);

        IList<Post> GetPostByTagName(string tagName);
    }
}
