﻿using JustBlog.Data.Infastructures;
using JustBlog.Models.Entity;
using System.Collections.Generic;

namespace JustBlog.Data.IRepositories
{
    public interface ITagRepository : IGenericRepository<Tag>
    {
        IEnumerable<int> AddTagByString(string tagNames);

        string GetTagByPost(int postId);
    }
}