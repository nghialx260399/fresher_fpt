﻿using JustBlog.Models.BaseEntity;
using JustBlog.ViewModels.BaseEntityVm;
using JustBlog.WebApp.Data;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace JustBlog.Data.Infastructures
{
    public abstract class GenericRepository<TEntity> : IGenericRepository<TEntity> where TEntity : class, IEntityBase
    {
        protected readonly ApplicationDbContext Context;

        protected DbSet<TEntity> DbSet;

        public GenericRepository(ApplicationDbContext context)
        {
            Context = context;
            DbSet = Context.Set<TEntity>();
        }

        public void Add(TEntity entity)
        {
            DbSet.Add(entity);
        }

        public void AddRange(IList<TEntity> entities)
        {
            DbSet.AddRange(entities);
        }

        public int CountTag(bool isDeleted = false)
        {
            if (!isDeleted)
                return this.DbSet.Count(x => !x.IsDeleted);

            return this.DbSet.Count();
        }

        public void Delete(bool isDeleted = false, params object[] keyValues)
        {
            var entityExisting = this.DbSet.Find(keyValues);
            if (entityExisting != null)
            {
                if (!isDeleted)
                {
                    entityExisting.IsDeleted = true;
                    this.Context.Entry(entityExisting).State = EntityState.Modified;

                    return;
                }

                this.DbSet.Remove(entityExisting);
            }
            throw new ArgumentNullException($"{string.Join(";", keyValues)} was not found in the {typeof(TEntity)}");
        }

        public void Delete(TEntity entity, bool isDeleted = false)
        {
            throw new NotImplementedException();
        }

        public void DeleteByCondition(Func<TEntity, bool> condition, bool isDeleted = false)
        {
            throw new NotImplementedException();
        }

        public List<TEntity> Find(Func<TEntity, bool> condition, bool isDeleted = false)
        {
            var query = this.DbSet.Where(condition);
            if (!isDeleted)
            {
                return query.Where(x => !x.IsDeleted).ToList();
            }
            return query.ToList();
        }

        public IList<TEntity> GetAll(bool isDeleted = false)
        {
            if (!isDeleted)
                return DbSet.Where(x => !x.IsDeleted).ToList();

            return DbSet.ToList();
        }

        public TEntity GetById(params object[] keyValues)
        {
            return DbSet.Find(keyValues);
        }

        public PagedVm<TEntity> GetPaging(Func<TEntity, bool> filter = null, 
            Func<IEnumerable<TEntity>, IOrderedEnumerable<TEntity>> orderBy = null, 
            bool isDeleted = false, string keyword = null, int pageIndex = 1, int pageSize = 10)
        {
            var query = this.DbSet.AsEnumerable();
            if (filter != null)
            {
                query = query.Where(filter);
            }
            if (!isDeleted)
            {
                query = query.Where(x => !x.IsDeleted);
            }

            int totalRows = query.Count();
            int totalPage = (int)Math.Ceiling((decimal)totalRows / pageSize);

            if (orderBy != null)
            {
                query = orderBy(query);
            }
            query = query.Skip((pageIndex - 1) * pageSize).Take(pageSize);


            return new PagedVm<TEntity>(query, pageIndex, pageSize, totalPage);
        }

        public void Update(TEntity entity)
        {
            DbSet.Update(entity);
        }
    }
}