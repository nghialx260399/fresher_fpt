﻿using JustBlog.Models.BaseEntity;
using JustBlog.ViewModels.BaseEntityVm;
using System;
using System.Collections.Generic;
using System.Linq;

namespace JustBlog.Data.Infastructures
{
    public interface IGenericRepository<TEntity> where TEntity : class, IEntityBase
    {
        IList<TEntity> GetAll(bool isDeleted = false);

        TEntity GetById(params object[] keyValues);

        void Add(TEntity entity);

        void AddRange(IList<TEntity> entities);

        void Update(TEntity entity);

        void Delete(bool isDeleted = false, params object[] keyValues);

        void Delete(TEntity entity, bool isDeleted = false);

        void DeleteByCondition(Func<TEntity, bool> condition, bool isDeleted = false);

        List<TEntity> Find(Func<TEntity, bool> condition, bool isDeleted = false);

        int CountTag(bool isDeleted = false);

        PagedVm<TEntity> GetPaging(Func<TEntity, bool> filter = null, Func<IEnumerable<TEntity>,
            IOrderedEnumerable<TEntity>> orderBy = null,
            bool isDeleted = false, string keyword = null, int pageIndex = 1, int pageSize = 10);
    }
}