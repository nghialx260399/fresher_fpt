﻿using JustBlog.Data.IRepositories;
using JustBlog.WebApp.Data;
using System;

namespace JustBlog.Data.Infastructures
{
    public interface IUnitOfWork: IDisposable
    {
        ICategoryRepository CategoryRepository { get; }

        IPostRepository PostRepository { get; }

        ITagRepository TagRepository { get; }

        ICommentRepository CommentRepository { get; }

        ApplicationDbContext ApplicationDbContext { get; }

        int SaveChanges();
    }
}