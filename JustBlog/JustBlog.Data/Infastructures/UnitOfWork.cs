﻿using JustBlog.Data.IRepositories;
using JustBlog.Data.Repositories;
using JustBlog.WebApp.Data;

namespace JustBlog.Data.Infastructures
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly ApplicationDbContext context;
        private ICategoryRepository categoryRepository;
        private IPostRepository postRepository;
        private ITagRepository tagRepository;
        private ICommentRepository commentRepository;

        public UnitOfWork(ApplicationDbContext context)
        {
            this.context = context;
        }

        public ICategoryRepository CategoryRepository => this.categoryRepository ??= new CategoryRepository(this.context);

        public IPostRepository PostRepository => this.postRepository ??= new PostRepository(this.context);

        public ITagRepository TagRepository => this.tagRepository ??= new TagRepository(this.context);

        public ApplicationDbContext ApplicationDbContext => this.context;

        public ICommentRepository CommentRepository => this.commentRepository ??= new CommentRepository(this.context);

        public void Dispose()
        {
            this.context.Dispose();
        }

        public int SaveChanges()
        {
            return this.context.SaveChanges();
        }
    }
}