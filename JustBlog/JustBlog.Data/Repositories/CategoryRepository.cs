﻿using JustBlog.Data.Infastructures;
using JustBlog.Data.IRepositories;
using JustBlog.Models.Entity;
using JustBlog.WebApp.Data;

namespace JustBlog.Data.Repositories
{
    public class CategoryRepository : GenericRepository<Category>, ICategoryRepository
    {
        public CategoryRepository(ApplicationDbContext context) : base(context)
        {
        }
    }
}