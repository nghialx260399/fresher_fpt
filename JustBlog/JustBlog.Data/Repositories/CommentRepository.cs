﻿using JustBlog.Data.Infastructures;
using JustBlog.Data.IRepositories;
using JustBlog.Models.Entity;
using JustBlog.WebApp.Data;

namespace JustBlog.Data.Repositories
{
    public class CommentRepository : GenericRepository<Comment>, ICommentRepository
    {
        public CommentRepository(ApplicationDbContext context) : base(context)
        {
        }
    }
}