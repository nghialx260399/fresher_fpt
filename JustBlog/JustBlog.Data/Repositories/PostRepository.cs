﻿using JustBlog.Data.Infastructures;
using JustBlog.Data.IRepositories;
using JustBlog.Models.Entity;
using JustBlog.ViewModels.Categories;
using JustBlog.WebApp.Data;
using System.Collections.Generic;
using System.Linq;

namespace JustBlog.Data.Repositories
{
    public class PostRepository : GenericRepository<Post>, IPostRepository
    {
        public PostRepository(ApplicationDbContext context) : base(context)
        {
        }

        public IList<Post> GetPostByTagName(string tagName)
        {
            var posts = from p in this.DbSet
                        join pt in this.Context.PostTagMaps
                        on p.Id equals pt.PostId
                        join t in this.Context.Tags
                        on pt.TagId equals t.Id
                        where t.Name.Equals(tagName)
                        select p;

            return posts.ToList();
        }

        public IList<Post> GetPostsByCategory(int id)
        {
            return this.DbSet.Where(x => x.CategoryId == id).ToList();
        }
    }
}