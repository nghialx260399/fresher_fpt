﻿using JustBlog.Data.Helper;
using JustBlog.Data.Infastructures;
using JustBlog.Data.IRepositories;
using JustBlog.Models.Entity;
using JustBlog.WebApp.Data;
using System.Collections.Generic;
using System.Linq;

namespace JustBlog.Data.Repositories
{
    public class TagRepository : GenericRepository<Tag>, ITagRepository
    {
        public TagRepository(ApplicationDbContext context) : base(context)
        {
        }

        public IEnumerable<int> AddTagByString(string tagNames)
        {
            var tags = tagNames.Split(';');
            foreach (var tag in tags)
            {
                var tagExisting = this.DbSet.Where(x => x.Name.ToLower().Trim().Equals(tag.ToLower().Trim())).FirstOrDefault();
                if (tagExisting == null)
                {
                    var newTag = new Tag()
                    {
                        Name = tag,
                        UrlSlug = UrlHelper.FrientlyUrl(tag),
                    };
                    this.DbSet.Add(newTag);
                }         
            }
            this.Context.SaveChanges();
            
            foreach (var tag in tags)
            {
                var tagExisting = this.DbSet.Where(x => x.Name.ToLower().Equals(tag.ToLower())).FirstOrDefault();
                if (tagExisting != null)
                    yield return tagExisting.Id;
            }
        }

        public string GetTagByPost(int postId)
        {
            var tags = from p in this.Context.Posts
                       join pt in this.Context.PostTagMaps
                       on p.Id equals pt.PostId
                       join t in this.DbSet
                       on pt.TagId equals t.Id
                       where p.Id == postId
                       select t.Name;

            return string.Join(";", tags);
        }
    }
}