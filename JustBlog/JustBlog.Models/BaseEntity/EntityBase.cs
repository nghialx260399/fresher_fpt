﻿using System;

namespace JustBlog.Models.BaseEntity
{
    public class EntityBase : IEntityBase
    {
        public bool IsDeleted { get; set; }

        public DateTime CreatedOn { get; set; }

        public DateTime UpdatedOn { get; set; }
    }
}