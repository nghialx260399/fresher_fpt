﻿using System;

namespace JustBlog.Models.BaseEntity
{
    public interface IEntityBase
    {
        bool IsDeleted { get; set; }

        DateTime CreatedOn { get; set; }

        DateTime UpdatedOn { get; set; }
    }
}