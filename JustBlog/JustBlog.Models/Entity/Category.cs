﻿using JustBlog.Models.BaseEntity;
using System.Collections.Generic;

namespace JustBlog.Models.Entity
{
    public class Category : EntityBase
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string UrlStug { get; set; }

        public string Description { get; set; }

        public IEnumerable<Post> Posts { get; set; }
    }
}