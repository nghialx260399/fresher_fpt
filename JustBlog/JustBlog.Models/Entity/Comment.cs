﻿using JustBlog.Models.BaseEntity;
using System;

namespace JustBlog.Models.Entity
{
    public class Comment : EntityBase
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Email { get; set; }

        public string CommentHeader { get; set; }

        public string CommentText { get; set; }

        public DateTime CommentTime { get; set; }

        public int PostId { get; set; }

        public Post Post { get; set; }
    }
}