﻿using JustBlog.Models.BaseEntity;
using System;
using System.Collections.Generic;

namespace JustBlog.Models.Entity
{
    public class Post : EntityBase
    {
        public int Id { get; set; }

        public string Title { get; set; }

        public string ShortDescription { get; set; }

        public string PostContent { get; set; }

        public string UrlSlug { get; set; }

        public DateTime Published { get; set; }

        public DateTime PostedOn { get; set; }

        public DateTime Modified { get; set; }

        public int CategoryId { get; set; }

        public int ViewCount { get; set; }

        public int RateCount { get; set; }

        public int TotalRate { get; set; }

        public decimal Rate { get; set; }

        public Category Category { get; set; }

        public IEnumerable<PostTagMap> PostTagMaps { get; set; }

        public IEnumerable<Comment> Comments { get; set; }
    }
}