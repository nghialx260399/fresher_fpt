﻿using JustBlog.Models.BaseEntity;
using System.Collections.Generic;

namespace JustBlog.Models.Entity
{
    public class Tag : EntityBase
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string UrlSlug { get; set; }

        public string Description { get; set; }

        public int Count { get; set; }

        public IEnumerable<PostTagMap> PostTagMaps { get; set; }
    }
}