﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JustBlog.ViewModels.Api
{
    public class PostFillter
    {
        public string Keyword { get; set; } = "";

        public int PageIndex { get; set; } = 1;

        public int PageSize { get; set; } = 5;
    }
}
