﻿using AutoMapper;
using JustBlog.Models.Entity;
using JustBlog.ViewModels.Posts;
using JustBlog.ViewModels.Tags;

namespace JustBlog.ViewModels.AutoMapper
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Post, PostVm>().ReverseMap();

            CreateMap<Tag, TagVm>().ReverseMap();
        }
    }
}