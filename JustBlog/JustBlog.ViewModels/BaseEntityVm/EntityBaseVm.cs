﻿using System;

namespace JustBlog.ViewModels.BaseEntityVm
{
    public class EntityBaseVm : IEntityBaseVm
    {
        public int Id { get; set; }

        public bool IsDeleted { get; set; }

        public DateTime CreatedOn { get; set; }

        public DateTime UpdatedOn { get; set; }
    }
}