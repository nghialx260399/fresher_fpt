﻿using System;

namespace JustBlog.ViewModels.BaseEntityVm
{
    public interface IEntityBaseVm
    {
        public int Id { get; set; }

        bool IsDeleted { get; set; }

        DateTime CreatedOn { get; set; }

        DateTime UpdatedOn { get; set; }
    }
}