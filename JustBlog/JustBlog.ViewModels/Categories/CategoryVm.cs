﻿using JustBlog.Models.Entity;
using JustBlog.ViewModels.BaseEntityVm;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace JustBlog.ViewModels.Categories
{
    public class CategoryVm : EntityBaseVm
    {
        [Required(ErrorMessage = "{0} is not reuqired")]
        public string Name { get; set; }

        [Required(ErrorMessage = "{0} is not reuqired")]
        public string UrlStug { get; set; }

        [Required(ErrorMessage = "{0} is not reuqired")]
        public string Description { get; set; }

        public IEnumerable<Post> Posts { get; set; }
    }
}