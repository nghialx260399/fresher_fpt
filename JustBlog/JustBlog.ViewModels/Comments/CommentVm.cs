﻿using JustBlog.ViewModels.BaseEntityVm;
using System;
using System.ComponentModel.DataAnnotations;

namespace JustBlog.ViewModels.Comments
{
    public class CommentVm : EntityBaseVm
    {
        public string Name { get; set; }

        public string Email { get; set; }

        public string CommentHeader { get; set; }

        [Required(ErrorMessage = "{0} is not required")]
        public string CommentText { get; set; }

        public DateTime CommentTime { get; set; }

        public int PostId { get; set; }
    }
}