﻿using JustBlog.Models.Entity;
using JustBlog.ViewModels.BaseEntityVm;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace JustBlog.ViewModels.Posts
{
    public class PostVm : EntityBaseVm
    {
        [Required(ErrorMessage = "{0} is not required")]
        public string Title { get; set; }

        [Required(ErrorMessage = "{0} is not required")]
        public string ShortDescription { get; set; }

        [Required(ErrorMessage = "{0} is not required")]
        public string PostContent { get; set; }

        [Required(ErrorMessage = "{0} is not required")]
        public string UrlSlug { get; set; }

        [Required(ErrorMessage = "{0} is not required")]
        [DataType(DataType.Date, ErrorMessage = "{0} must date")]
        public DateTime Published { get; set; }

        [Required(ErrorMessage = "{0} is not required")]
        [DataType(DataType.Date, ErrorMessage = "{0} must date")]
        public DateTime PostedOn { get; set; }

        [Required(ErrorMessage = "{0} is not required")]
        [DataType(DataType.Date, ErrorMessage = "{0} must date")]
        public DateTime Modified { get; set; }

        [Required(ErrorMessage = "{0} is not required")]
        [DisplayName("Category")]
        public int CategoryId { get; set; }

        [Range(0, int.MaxValue)]
        public int ViewCount { get; set; }

        [Range(0, int.MaxValue)]
        public int RateCount { get; set; }

        [Range(0, int.MaxValue)]
        public int TotalRate { get; set; }

        public decimal Rate { get; set; }

        [Required(ErrorMessage = "{0} is not required")]
        public string Tags { get; set; }

        public string Comment { get; set; }

        public Category Category { get; set; }

        public IEnumerable<PostTagMap> PostTagMaps { get; set; }

        public IEnumerable<Comment> Comments { get; set; }
    }
}