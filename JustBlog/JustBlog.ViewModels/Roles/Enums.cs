﻿namespace JustBlog.ViewModels.Roles
{
    public class Enums
    {
        public enum Roles
        {
            SuperAdmin,
            Admin,
            Moderator,
            Basic
        }
    }
}