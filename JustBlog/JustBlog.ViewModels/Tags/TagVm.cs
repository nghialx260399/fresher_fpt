﻿using JustBlog.ViewModels.BaseEntityVm;
using System.ComponentModel.DataAnnotations;

namespace JustBlog.ViewModels.Tags
{
    public class TagVm : EntityBaseVm
    {
        [Required(ErrorMessage = "{0} not required")]
        public string Name { get; set; }

        public string UrlSlug { get; set; }

        public string Description { get; set; }

        public int Count { get; set; }
    }
}