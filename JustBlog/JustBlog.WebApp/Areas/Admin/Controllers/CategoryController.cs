﻿using JustBlog.Application.Categories;
using JustBlog.ViewModels.Categories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JustBlog.WebApp.Areas.Admin.Controllers
{
    public class CategoryController : BaseController
    {
        private readonly ILogger<CategoryController> logger;
        private readonly ICategoryService categoryService;

        public CategoryController(ILogger<CategoryController> logger, ICategoryService categoryService)
        {
            this.logger = logger;
            this.categoryService = categoryService;
        }

        public IActionResult Index()
        {
            if (TempData["InforCreate"] != null)
                TempData["Success"] = TempData["InforCreate"];

            if (TempData["InforUpdate"] != null)
                TempData["Success"] = TempData["InforUpdate"];

            if (TempData["InforDelete"] != null)
                TempData["Success"] = TempData["InforDelete"];

            var categories = categoryService.GetAll(true);

            return View(categories);
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Create(CategoryVm categoryVm)
        {
            if (!ModelState.IsValid)
            {
                TempData["InforCreate"] = "Create fail";
                return View(categoryVm);
            }

            var isSuccess = categoryService.Create(categoryVm);

            if (isSuccess)
            {
                TempData["InforCreate"] = "Create success";
                return RedirectToAction("Index", "Category");
            }
            return View(categoryVm);
        }

        public IActionResult Edit(int id)
        {
            var categoryVm = categoryService.GetById(id);

            if(categoryVm != null)
                return View(categoryVm);

            return RedirectToAction("Index", "Category");
        }

        [HttpPost]
        public IActionResult Edit(CategoryVm categoryVm)
        {
            if (!ModelState.IsValid)
            {
                TempData["InforUpdate"] = "Update fail";
                return View(categoryVm);
            }

            var isSuccess = categoryService.Update(categoryVm);

            if (isSuccess)
            {
                TempData["InforUpdate"] = "Update success";
                return RedirectToAction("Index", "Category");
            }
            return View(categoryVm);
        }

        [Authorize(Roles = "Blog Owner")]
        public IActionResult Delete(int id)
        {
            var categoryVm = categoryService.GetById(id);

            if (categoryVm != null)
                return View(categoryVm);

            return RedirectToAction("Index", "Category");
        }

        [Authorize(Roles = "Blog Owner")]
        [HttpPost]
        public IActionResult Delete(int id, IFormCollection collection)
        {
            if (!ModelState.IsValid)
                TempData["InforDelete"] = "Delete fail";

            var isSuccess = categoryService.Delete(id);

            if (isSuccess)
            {
                TempData["InforDelete"] = "Delete success";
                return RedirectToAction("Index", "Category");
            }

            return View();
        }
    }
}
