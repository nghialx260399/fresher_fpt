﻿using JustBlog.Application.Comments;
using JustBlog.Models.Entity;
using JustBlog.ViewModels.Comments;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JustBlog.WebApp.Areas.Admin.Controllers
{
    public class CommentController : BaseController
    {
        private readonly ILogger<CommentController> logger;
        private readonly ICommentService commentService;

        public CommentController(ILogger<CommentController> logger, ICommentService commentService)
        {
            this.logger = logger;
            this.commentService = commentService;
        }

        public IActionResult Index(string keyword = null, int pageIndex = 1, int pageSize = 5)
        {
            if (TempData["InforCreate"] != null)
                TempData["Success"] = TempData["InforCreate"];

            if (TempData["InforUpdate"] != null)
                TempData["Success"] = TempData["InforUpdate"];

            if (TempData["InforDelete"] != null)
                TempData["Success"] = TempData["InforDelete"];

            Func<Comment, bool> filter = null;
            if (!string.IsNullOrEmpty(keyword))
            {
                ViewBag.Keyword = keyword;
                keyword = keyword.ToLower();
                filter = x => x.Email.ToLower().Contains(keyword);
            }
            var commentPaged = this.commentService.GetPaging(filter: filter, pageIndex: pageIndex, pageSize: pageSize);
            return View(commentPaged);
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Create(CommentVm commentVm)
        {
            if (!ModelState.IsValid)
            {
                TempData["InforCreate"] = "Create fail";
                return View(commentVm);
            }
            var isSuccess = this.commentService.Create(commentVm);
            if (isSuccess)
            {
                TempData["InforCreate"] = "Create success";
                return RedirectToAction("Index", "Comment");
            }
            return View();
        }

        [Authorize(Roles = "Blog Owner")]
        public IActionResult Delete(int id)
        {
            try
            {
                var commentVm = this.commentService.GetById(id);

                return View(commentVm);
            }
            catch
            {
                return RedirectToAction("Index", "Comment");
            }
        }

        [Authorize(Roles = "Blog Owner")]
        [HttpPost]
        public IActionResult Delete(int id, IFormCollection collection)
        {
            if (!ModelState.IsValid)
                TempData["InforDelete"] = "Delete fail";

            var isSuccess = this.commentService.Delete(id);

            if (isSuccess)
            {
                TempData["InforDelete"] = "Delete success";
                return RedirectToAction("Index", "Comment");
            }

            return View();
        }
    }
}
