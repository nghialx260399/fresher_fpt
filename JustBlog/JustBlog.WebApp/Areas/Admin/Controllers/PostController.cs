﻿using JustBlog.Application.Posts;
using JustBlog.Models.Entity;
using JustBlog.ViewModels.Categories;
using JustBlog.ViewModels.Posts;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JustBlog.WebApp.Areas.Admin.Controllers
{
    public class PostController : BaseController
    {
        private readonly ILogger<PostController> logger;
        private readonly IPostService postService;

        public PostController(ILogger<PostController> logger, IPostService postService)
        {
            this.logger = logger;
            this.postService = postService;
        }

        public IActionResult Index(string keyword = null, int pageIndex = 1, int pageSize = 5)
        {
            if (TempData["InforCreate"] != null)
                TempData["Success"] = TempData["InforCreate"];

            if (TempData["InforUpdate"] != null)
                TempData["Success"] = TempData["InforUpdate"];

            if (TempData["InforDelete"] != null)
                TempData["Success"] = TempData["InforDelete"];

            Func<Post, bool> filter = null;
            if (!string.IsNullOrEmpty(keyword))
            {
                ViewBag.Keyword = keyword;
                keyword = keyword.ToLower();
                filter = x => x.Title.ToLower().Contains(keyword);
            }
            var postPaged = this.postService.GetPaging(filter: filter,isDeleted:true, pageIndex: pageIndex, pageSize: pageSize);
            return View(postPaged);
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Create(PostVm postVm)
        {
            if (!ModelState.IsValid)
            {
                TempData["InforCreate"] = "Create fail";
                return View(postVm);
            }
            var isSuccess = this.postService.Create(postVm);
            if (isSuccess)
            {
                TempData["InforCreate"] = "Create success";
                return RedirectToAction("Index", "Post");
            }
            return View();
        }

        public IActionResult Edit(int id)
        {
            try
            {
                var postVm = postService.GetById(id);

                return View(postVm);
            }
            catch
            {
                return RedirectToAction("Index", "Post");
            }

        }

        [HttpPost]
        public IActionResult Edit(PostVm postVm)
        {
            if (!ModelState.IsValid)
            {
                TempData["InforUpdate"] = "Update fail";
                return View(postVm);
            }

            var isSuccess = postService.Update(postVm);

            if (isSuccess)
            {
                TempData["InforUpdate"] = "Update success";
                return RedirectToAction("Index", "Post");
            }
            return View(postVm);
        }

        [Authorize(Roles = "Blog Owner")]
        public IActionResult Delete(int id)
        {
            try
            {
                var postVm = postService.GetById(id);

                return View(postVm);
            }
            catch
            {
                return RedirectToAction("Index", "Post");
            }
        }

        [Authorize(Roles = "Blog Owner")]
        [HttpPost]
        public IActionResult Delete(int id, IFormCollection collection)
        {
            if (!ModelState.IsValid)
                TempData["InforDelete"] = "Delete fail";

            var isSuccess = postService.Delete(id);

            if (isSuccess)
            {
                TempData["InforDelete"] = "Delete success";
                return RedirectToAction("Index", "Post");
            }

            return View();
        }

        [HttpGet]
        public IActionResult Publish(int id)
        {
            if (!ModelState.IsValid)
                TempData["InforDelete"] = "Publish fail";

            var isSuccess = postService.Publish(id);

            if (isSuccess)
            {
                TempData["InforDelete"] = "Publish success";
                return RedirectToAction("Index", "Post");
            }

            return View();
        }


        [HttpGet]
        public IActionResult UnPublish(int id)
        {
            if (!ModelState.IsValid)
                TempData["InforDelete"] = "UnPublish fail";

            var isSuccess = postService.UnPublish(id);

            if (isSuccess)
            {
                TempData["InforDelete"] = "UnPublish success";
                return RedirectToAction("Index", "Post");
            }

            return View();
        }
    }
}
