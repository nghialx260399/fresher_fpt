﻿using JustBlog.Application.Tags;
using JustBlog.Models.Entity;
using JustBlog.ViewModels.Categories;
using JustBlog.ViewModels.Tags;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JustBlog.WebApp.Areas.Admin.Controllers
{
    public class TagController : BaseController
    {
        private readonly ILogger<TagController> logger;
        private readonly ITagService tagService;

        public TagController(ILogger<TagController> logger, ITagService tagService)
        {
            this.logger = logger;
            this.tagService = tagService;
        }

        public IActionResult Index(string keyword = null, int pageIndex = 1, int pageSize = 5)
        {
            if (TempData["InforCreate"] != null)
                TempData["Success"] = TempData["InforCreate"];

            if (TempData["InforUpdate"] != null)
                TempData["Success"] = TempData["InforUpdate"];

            if (TempData["InforDelete"] != null)
                TempData["Success"] = TempData["InforDelete"];

            Func<Tag, bool> filter = null;
            if (!string.IsNullOrEmpty(keyword))
            {
                ViewBag.Keyword = keyword;
                keyword = keyword.ToLower();
                filter = x => x.Name.ToLower().Contains(keyword);
            }
            var tagPaged = this.tagService.GetPaging(filter: filter, pageIndex: pageIndex, pageSize: pageSize);
            return View(tagPaged);
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Create(TagVm tagVm)
        {
            if (!ModelState.IsValid)
            {
                TempData["InforCreate"] = "Create fail";
                return View(tagVm);
            }

            var isSuccess = tagService.Create(tagVm);

            if (isSuccess)
            {
                TempData["InforCreate"] = "Create success";
                return RedirectToAction("Index", "Tag");
            }
            return View(tagVm);
        }

        public IActionResult Edit(int id)
        {
            try
            {
                var tagVm = tagService.GetById(id);

                return View(tagVm);
            }
            catch
            {
                return RedirectToAction("Index", "Tag");
            }

        }

        [HttpPost]
        public IActionResult Edit(TagVm tagVm)
        {
            if (!ModelState.IsValid)
            {
                TempData["InforUpdate"] = "Update fail";
                return View(tagVm);
            }

            var isSuccess = tagService.Update(tagVm);

            if (isSuccess)
            {
                TempData["InforUpdate"] = "Update success";
                return RedirectToAction("Index", "Tag");
            }
            return View(tagVm);
        }

        [Authorize(Roles = "Blog Owner")]
        public IActionResult Delete(int id)
        {
            try
            {
                var tagVm = tagService.GetById(id);

                return View(tagVm);
            }
            catch
            {
                return RedirectToAction("Index", "Tag");
            }
        }

        [Authorize(Roles = "Blog Owner")]
        [HttpPost]
        public IActionResult Delete(int id, IFormCollection collection)
        {
            if (!ModelState.IsValid)
                TempData["InforDelete"] = "Delete fail";

            var isSuccess = tagService.Delete(id);

            if (isSuccess)
            {
                TempData["InforDelete"] = "Delete success";
                return RedirectToAction("Index", "Tag");
            }

            return View();
        }
    }
}
