﻿using JustBlog.Application.Posts;
using JustBlog.Models.Entity;
using JustBlog.WebApp.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Diagnostics;

namespace JustBlog.WebApp.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IPostService postService;

        public HomeController(ILogger<HomeController> logger, IPostService postService)
        {
            _logger = logger;
            this.postService = postService;
        }

        public IActionResult Index(string keyword = null, int pageIndex = 1, int pageSize = 5)
        {
            if (TempData["InforCreate"] != null)
                TempData["Success"] = TempData["InforCreate"];

            if (TempData["InforUpdate"] != null)
                TempData["Success"] = TempData["InforUpdate"];

            if (TempData["InforDelete"] != null)
                TempData["Success"] = TempData["InforDelete"];

            Func<Post, bool> filter = null;
            if (!string.IsNullOrEmpty(keyword))
            {
                ViewBag.Keyword = keyword;
                keyword = keyword.ToLower();
                filter = x => x.Title.ToLower().Contains(keyword);
            }
            var postPaged = this.postService.GetPaging(filter: filter, isDeleted: true, pageIndex: pageIndex, pageSize: pageSize);
            return View(postPaged);
        }

        public IActionResult AboutCard()
        {
            return PartialView("_About");
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}