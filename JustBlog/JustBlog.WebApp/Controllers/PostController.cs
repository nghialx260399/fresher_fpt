﻿using JustBlog.Application.Comments;
using JustBlog.Application.Posts;
using JustBlog.Application.Tags;
using JustBlog.Models.Entity;
using JustBlog.ViewModels.Comments;
using JustBlog.ViewModels.Posts;
using JustBlog.WebApp.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;

namespace JustBlog.WebApp.Controllers
{
    public class PostController : Controller
    {
        private readonly ILogger<PostController> logger;
        private readonly IPostService postService;
        private readonly ICommentService commentService;
        private readonly UserManager<ApplicationUser> userManager;
        private readonly ITagService tagService;

        public PostController(ILogger<PostController> logger, IPostService postService,
            ICommentService commentService, UserManager<ApplicationUser> UserManager,
            ITagService tagService)
        {
            this.logger = logger;
            this.postService = postService;
            this.commentService = commentService;
            userManager = UserManager;
            this.tagService = tagService;
        }

        public IActionResult MostViewedPosts()
        {
            var mostView = postService.GetAll().OrderByDescending(x => x.ViewCount).Take(5);

            return PartialView("_ListPosts", mostView);
        }

        public IActionResult LatesPosts()
        {
            var postLates = postService.GetAll().OrderByDescending(x => x.PostedOn).Take(5);

            return PartialView("_ListPosts", postLates);
        }

        public IActionResult GetPostsByTagName(string tagName)
        {
            var posts = postService.GetPostsByTag(tagName);
            this.tagService.UpdateTagByTagName(tagName);

            return PartialView("_ListPosts", posts);
        }

        public IActionResult GetPostsByCategory(int id, int pageIndex = 1, int pageSize = 5)
        {
            Func<Post, bool> filter = x=>x.CategoryId == id;
            var postPaged = this.postService.GetPaging(filter: filter, isDeleted: true, pageIndex: pageIndex, pageSize: pageSize);

            return PartialView("_ListPosts", postPaged);
        }

        public IActionResult PostDetails(int id)
        {
            TempData["PostDetailId"] = id;
            var post = postService.GetById(id);

            post.ViewCount++;
            postService.Update(post);

            return View(post);
        }

        //[HttpPost]
        //[Authorize(Roles = "User, Contributor, Blog Owner")]
        //public IActionResult AddComment(PostVm postVm)
        //{
        //    try
        //    {
        //        if (string.IsNullOrEmpty(postVm.Comment.Trim()))
        //            return RedirectToAction("PostDetails", new { id = postVm.Id });

        //        var commentVm = new CommentVm();

        //        commentVm.CommentText = postVm.Comment.Trim();
        //        commentVm.PostId = postVm.Id;
        //        commentVm.Name = userManager.GetUserAsync(User).Result.UserName;
        //        commentVm.Email = userManager.GetUserAsync(User).Result.Email;

        //        commentService.Create(commentVm);

        //        return RedirectToAction("PostDetails", new { id = postVm.Id });
        //    }
        //    catch
        //    {
        //        return RedirectToAction("PostDetails", new { id = postVm.Id });
        //    }
        //}

        [HttpPost]
        [Authorize(Roles = "User, Contributor, Blog Owner")]
        public void AddComment(string commentText, int postId)
        {
            var commentVm = new CommentVm();

            commentVm.CommentText = commentText.Trim();
            commentVm.PostId = postId;
            commentVm.Name = userManager.GetUserAsync(User).Result.UserName;
            commentVm.Email = userManager.GetUserAsync(User).Result.Email;

            commentService.Create(commentVm);
        }
    }
}