﻿using JustBlog.Application.Tags;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JustBlog.WebApp.Controllers
{
    public class TagController : Controller
    {
        private readonly ITagService tagService;

        public TagController(ITagService tagService)
        {
            this.tagService = tagService;
        }

        public IActionResult PopularTags()
        {
            var tagTop = tagService.GetAll().OrderByDescending(x => x.Count).Take(10);
            
            return PartialView("_PopularTags", tagTop);
        }
    }
}
