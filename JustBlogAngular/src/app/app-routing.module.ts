import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreateCategoryComponent } from './create-category/create-category.component';
import { CreatePostComponent } from './create-post/create-post.component';
import { CreateTagComponent } from './create-tag/create-tag.component';
import { GetcategoryComponent } from './getcategory/getcategory.component';
import { GetcommentComponent } from './getcomment/getcomment.component';
import { GetpostComponent } from './getpost/getpost.component';
import { GettagComponent } from './gettag/gettag.component';
import { LoginComponent } from './login/login.component';
import { UpdateCategoryComponent } from './update-category/update-category.component';
import { UpdatePostComponent } from './update-post/update-post.component';
import { UpdateTagComponent } from './update-tag/update-tag.component';

const routes: Routes = [
  {path:'category',component:GetcategoryComponent},
  {path:'create-category', component: CreateCategoryComponent},
  {path:'edit-category/:id', component: UpdateCategoryComponent},
  {path:'post',component:GetpostComponent},
  {path:'create-post', component: CreatePostComponent},
  {path:'edit-post/:id', component: UpdatePostComponent},
  {path:'tag',component:GettagComponent},
  {path:'create-tag', component: CreateTagComponent},
  {path:'edit-tag/:id', component: UpdateTagComponent},
  {path:'comment',component:GetcommentComponent},
  {path:'login',component:LoginComponent},
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
