import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { GetcategoryComponent } from './getcategory/getcategory.component';
import { CreateCategoryComponent } from './create-category/create-category.component';
import { UpdateCategoryComponent } from './update-category/update-category.component';
import { GetpostComponent } from './getpost/getpost.component';
import { CreatePostComponent } from './create-post/create-post.component';
import { UpdatePostComponent } from './update-post/update-post.component';
import { GettagComponent } from './gettag/gettag.component';
import { CreateTagComponent } from './create-tag/create-tag.component';
import { UpdateTagComponent } from './update-tag/update-tag.component';
import { GetcommentComponent } from './getcomment/getcomment.component';
import { NotifierModule } from 'angular-notifier';
import { customNotifierOptions } from './shared/notification-config';
import { TokenInterceptorService } from './shared/token-interceptor.service';
import { ErrorInterceptorService } from './shared/error-interceptor.service';
import { LoginComponent } from './login/login.component';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';

@NgModule({
  declarations: [
    AppComponent,
    GetcategoryComponent,
    CreateCategoryComponent,
    UpdateCategoryComponent,
    GetpostComponent,
    CreatePostComponent,
    UpdatePostComponent,
    GettagComponent,
    CreateTagComponent,
    UpdateTagComponent,
    GetcommentComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    NotifierModule.withConfig(customNotifierOptions),
    CKEditorModule,
  ],
  providers: [
    {
      provide:HTTP_INTERCEPTORS,
      useClass:TokenInterceptorService,
      multi:true
    },
    {
      provide:HTTP_INTERCEPTORS,
      useClass:ErrorInterceptorService,
      multi:true
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
