import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { CategoryService } from '../shared/category.service';

@Component({
  selector: 'app-create-category',
  templateUrl: './create-category.component.html',
  styleUrls: ['./create-category.component.css']
})
export class CreateCategoryComponent implements OnInit {

  category: any;
  categoryForm: FormGroup;
  constructor(private route: ActivatedRoute, private categoryService: CategoryService, private fb: FormBuilder,private router:Router) {
    this.categoryForm = this.fb.group({
      name:['', Validators.required],
      urlStug:['', Validators.required],
      description:['', Validators.required],
    });
   }

  ngOnInit(): void {
  }

  Create(){
    if(!this.categoryForm.invalid){
      this.categoryService.addCategory(this.categoryForm.value).subscribe(res=>{
        if(res.status==200){
          alert('Add is successfully');
          this.router.navigateByUrl('/category');
        }
        else{
          alert('Failed');
        }
       })
    }
    else{
      alert('Validate form error');
    }

  }

  get name(){
    return this.categoryForm.get('name');
  }

  get urlStug(){
    return this.categoryForm.get('urlStug');
  }

  get description(){
    return this.categoryForm.get('description');
  }

}
