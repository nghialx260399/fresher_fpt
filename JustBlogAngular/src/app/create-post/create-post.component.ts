import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { CategoryService } from '../shared/category.service';
import { PostService } from '../shared/post.service';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';

@Component({
  selector: 'app-create-post',
  templateUrl: './create-post.component.html',
  styleUrls: ['./create-post.component.css']
})

export class CreatePostComponent implements OnInit {

  post: any;
  postForm: FormGroup;
  categories:any;
  Editor:any;

  constructor(private route: ActivatedRoute, private postService: PostService, private fb: FormBuilder,private router:Router, private categoryService:CategoryService) {
    this.postForm = this.fb.group({
      title:['', Validators.required],
      shortDescription:['', Validators.required],
      urlSlug:['', Validators.required],
      tags:['', Validators.required],
      categoryId:['', Validators.required],
      postContent:['', Validators.required],
    });
    this.Editor = ClassicEditor;
   }

  ngOnInit(): void {
    this.getCategories();
  }

  getCategories(){
    this.categoryService.getCategories().subscribe(res=>{
      console.log(res);
     if(res.status==200){
       this.categories=res.body;
     }
     else{
       alert('Get data from serve  failed');
     }
    })
  }

  Create(){
    if(!this.postForm.invalid){
      this.postService.addPost(this.postForm.value).subscribe(res=>{
        if(res.status==200){
          alert('Add is successfully');
          this.router.navigateByUrl('/post');
        }
        else{
          alert('Failed');
        }
       })
    }
    else{
      alert('Validate form error');
    }

  }

  get title(){
    return this.postForm.get('title');
  }

  get shortDescription(){
    return this.postForm.get('shortDescription');
  }

  get urlSlug(){
    return this.postForm.get('urlSlug');
  }

  get tags(){
    return this.postForm.get('tags');
  }

  get categoryId(){
    return this.postForm.get('categoryId');
  }

  get postContent(){
    return this.postForm.get('postContent');
  }
}
