import { Component, OnInit } from '@angular/core';
import { CategoryService } from '../shared/category.service';

@Component({
  selector: 'app-getcategory',
  templateUrl: './getcategory.component.html',
  styleUrls: ['./getcategory.component.css']
})
export class GetcategoryComponent implements OnInit {

  categories:any;

  constructor(private categoryService:CategoryService) { }

  ngOnInit(): void {
    this.getCategories();
  }

  getCategories(){
    this.categoryService.getCategories().subscribe(res=>{
      console.log(res);
     if(res.status==200){
       this.categories=res.body;
     }
     else{
       alert('Get data from serve  failed');
     }
    })
  }

  deleteCategory(id:string){
    this.categoryService.deleteCategory(id).subscribe(res=>{
    console.log(res);
      if(res.status==200){
        alert('Delete successfully');
        this.getCategories();
      }
      else{
        alert('Failed!');
      }
    })
  }
}
