import { Component, OnInit } from '@angular/core';
import { CommentService } from '../shared/comment.service';

@Component({
  selector: 'app-getcomment',
  templateUrl: './getcomment.component.html',
  styleUrls: ['./getcomment.component.css']
})
export class GetcommentComponent implements OnInit {
  comments:any;

  constructor(private commentService:CommentService) { }
  ngOnInit(): void {
    this.getComments();
  }

  getComments(){
    this.commentService.getComments().subscribe(res=>{
      console.log(res);
     if(res.status==200){
       this.comments=res.body;
     }
     else{
       alert('Get data from serve  failed');
     }
    })
  }

  deleteComment(id:string){
    
  }

}
