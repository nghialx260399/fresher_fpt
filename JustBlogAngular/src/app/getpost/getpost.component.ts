import { Component, OnInit } from '@angular/core';
import { PostService } from '../shared/post.service';

@Component({
  selector: 'app-getpost',
  templateUrl: './getpost.component.html',
  styleUrls: ['./getpost.component.css']
})
export class GetpostComponent implements OnInit {

  posts:any;
  paged:any;
  pageIndex:any;
  pageSize:any;
  pageTotal:any;
  fakeArray:any;

  constructor(private postService:PostService) { }

  ngOnInit(): void {
    this.getPosts(1, null);
  }

  getPosts(pageIndex:any, search:any){
    this.postService.getPosts(pageIndex, search).subscribe(res=>{
      console.log(res);
     if(res.status==200){
       this.paged=res.body;
       this.posts = this.paged.data;
       this.pageIndex = this.paged.pageIndex;
       this.pageSize = this.paged.pageSize;
       this.pageTotal = this.paged.totalPage;
       this.fakeArray = new Array(this.pageTotal);
     }
     else{
       alert('Get data from serve  failed');
     }
    })
  }

  deletePost(id:string){
    this.postService.deletePost(id).subscribe(res=>{
    console.log(res);
      if(res.status==200){
        alert('Delete successfully');
        this.getPosts(1, null);
      }
      else{
        alert('Failed!');
      }
    })
  }

}
