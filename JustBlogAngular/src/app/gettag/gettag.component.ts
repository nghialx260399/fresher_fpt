import { Component, OnInit } from '@angular/core';
import { TagService } from '../shared/tag.service';

@Component({
  selector: 'app-gettag',
  templateUrl: './gettag.component.html',
  styleUrls: ['./gettag.component.css']
})
export class GettagComponent implements OnInit {

  tags:any;

  constructor(private tagService:TagService) { }

  ngOnInit(): void {
    this.getTags();
  }

  getTags(){
    this.tagService.getTags().subscribe(res=>{
      console.log(res);
     if(res.status==200){
       this.tags=res.body;
     }
     else{
       alert('Get data from serve  failed');
     }
    })
  }

  deleteTag(id:string){
    
  }

}
