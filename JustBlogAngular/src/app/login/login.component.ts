import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../shared/auth.service';
import { NotificationService } from '../shared/notification.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {

  loginForm:FormGroup;

  constructor(private fb:FormBuilder,private authService:AuthService,private route:Router) {
    this.loginForm= this.fb.group({
      email:[''],
      password:['']
    })
  }

  submit(){
    this.authService.login(this.loginForm.value).subscribe(res=>{
      localStorage.setItem('token',res);
      this.route.navigateByUrl('/');
    })
  }

}
