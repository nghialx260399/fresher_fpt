import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  apiUrl="https://localhost:44394/api";
  constructor(private httpClient:HttpClient, private route:Router) { }

  login(formData:any){
    var option={
      headers:new HttpHeaders({
        'Content-Type':'application/json'
      }),
      responseType:'text' as const
    }
    return this.httpClient.post(this.apiUrl+'/Account/Login',formData,option);
  }

    getToken(){
     return localStorage.getItem('token');
    }

    navigateToLogin(){
      this.route.navigateByUrl('/login');
    }
}
