import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { category } from './model';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  constructor(private httpClient:HttpClient) { }

  getCategories(): Observable<HttpResponse<category[]>> {
    const apiUrl = 'https://localhost:44394/api/Category/GetAll';

    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      }),
      observe: 'response' as const
    }
    return this.httpClient.get<category[]>(apiUrl, options);
  }

  getCategoryById(id:number): Observable<HttpResponse<category[]>> {
    const apiUrl = `https://localhost:44394/api/Category/GetById/${id}`;
    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      }),
      observe: 'response' as const
    }
    return this.httpClient.get<category[]>(apiUrl, options);
  }

  updateCategory(formData:any){
    var data= JSON.stringify(formData);
    const apiUrl= `https://localhost:44394/api/Category/Put`;
    var options={
      headers: new HttpHeaders({
        'Content-Type':'application/json'
      }),
      observe:'response' as const
    }
    return this.httpClient.put(apiUrl,data, options);

  }

  addCategory(formData:any){
    var data= JSON.stringify(formData);
    const apiUrl= 'https://localhost:44394/api/Category/Post';
    var options={
      headers: new HttpHeaders({
        'Content-Type':'application/json'
      }),
      observe:'response' as const
    }
    return this.httpClient.post(apiUrl,data, options);

  }

  deleteCategory(id: string) {
    const apiUrl = `https://localhost:44394/api/Category/Delete/${id}`;
    return this.httpClient.delete(apiUrl, { observe: 'response' });
  }
}
