import { HttpEvent, HttpHandler, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import {retry, catchError} from 'rxjs/operators';
import { AuthService } from './auth.service';
import { NotificationService } from './notification.service';

@Injectable({
  providedIn: 'root'
})
export class ErrorInterceptorService {

  constructor(private noticeService:NotificationService,private authService:AuthService) {


  }
  intercept(request: HttpRequest<any>,next: HttpHandler): Observable<HttpEvent<any>>{

    return next.handle(request).pipe(
        retry(1),
        catchError((err)=>{
            if(err.status==401){
               this.noticeService.show('error','Ban khong co quyen truy cap');
               this.authService.navigateToLogin();
            }
            else if(err.status==404){
              alert('status code 404')
                console.log(err);
            }
            else if(err.status==403){
              this.noticeService.show('error','Ban khong co quyen truy cap');
            }

            else{
              this.noticeService.show('error','Login fail');
            }
            return throwError(err)
        })
    )
  }
}
