export interface category{
    id:number;
    name:string;
    urlStug:string;
    description:string;
    isDeleted:boolean;
    createdOn:Date;
    updatedOn:Date;
}

export interface comment{
    id:number;
    postId:number;
    name:string;
    email:string;
    commentText:string;
    commentTime:Date;
    isDeleted:boolean;
    createdOn:Date;
    updatedOn:Date;
}

export interface post{
    id:number;
    title:string;
    shortDescription:string;
    postContent:string;
    urlSlug:string;
    published:Date;
    postedOn:Date;
    categoryId:number;
    viewCount:number;
    isDeleted:boolean;
    createdOn:Date;
    updatedOn:Date;
}

export interface paged{
    data:post[];
    pageIndex:number;
    pageSize:number;
    totalPage:number;
}
export interface tag{
    id:number;
    name:string;
    urlStug:string;
    description:string;
    count:number;
    isDeleted:boolean;
    createdOn:Date;
    updatedOn:Date;
}