import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { paged, post } from './model';

@Injectable({
  providedIn: 'root'
})
export class PostService {

  constructor(private httpClient:HttpClient) { }

  getPosts(pageIndex:any, search:any): Observable<HttpResponse<paged[]>> {
    // const apiUrl = `https://localhost:44394/api/Post/GetAll?PageIndex=${pageIndex}`;
    var apiUrl;
    search == null ? apiUrl = `https://localhost:44394/api/Post/GetAll?PageIndex=${pageIndex}` 
                    : apiUrl = `https://localhost:44394/api/Post/GetAll?PageIndex=${pageIndex}&Keyword=${search.value}`

    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      }),
      observe: 'response' as const
    }
    return this.httpClient.get<paged[]>(apiUrl, options);
  }

  addPost(formData:any){
    var data= JSON.stringify(formData);
    const apiUrl= 'https://localhost:44394/api/Post/Post';
    var options={
      headers: new HttpHeaders({
        'Content-Type':'application/json'
      }),
      observe:'response' as const
    }
    return this.httpClient.post(apiUrl,data, options);

  }

  updatePost(formData:any){
    var data= JSON.stringify(formData);
    const apiUrl= `https://localhost:44394/api/Post/Put`;
    var options={
      headers: new HttpHeaders({
        'Content-Type':'application/json'
      }),
      observe:'response' as const
    }
    return this.httpClient.put(apiUrl,data, options);

  }

  getPostById(id:number): Observable<HttpResponse<post[]>> {
    const apiUrl = `https://localhost:44394/api/Post/GetById/${id}`;
    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      }),
      observe: 'response' as const
    }
    return this.httpClient.get<post[]>(apiUrl, options);
  }

  deletePost(id: string) {
    const apiUrl = `https://localhost:44394/api/Post/Delete/${id}`;
    return this.httpClient.delete(apiUrl, { observe: 'response' });
  }
}
