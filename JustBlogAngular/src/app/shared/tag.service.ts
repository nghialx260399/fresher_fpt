import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { tag } from './model';

@Injectable({
  providedIn: 'root'
})
export class TagService {

  constructor(private httpClient:HttpClient) { }

  getTags(): Observable<HttpResponse<tag[]>> {
    const apiUrl = 'https://localhost:44394/api/Tag/GetAll';

    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      }),
      observe: 'response' as const
    }
    return this.httpClient.get<tag[]>(apiUrl, options);
  }
}
