import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { CategoryService } from '../shared/category.service';
import { NotificationService } from '../shared/notification.service';

@Component({
  selector: 'app-update-category',
  templateUrl: './update-category.component.html',
  styleUrls: ['./update-category.component.css']
})
export class UpdateCategoryComponent implements OnInit {

  id: any;
  category: any;
  categoryForm: FormGroup;
  constructor(private route: ActivatedRoute, private categoryService: CategoryService, private fb: FormBuilder,private router:Router,
    private noticeService:NotificationService) {
    this.id = this.route.snapshot.paramMap.get('id');
    this.getCategoryById();
    this.categoryForm = this.fb.group({
      id:"",
      name:['', Validators.required],
      urlStug:['', Validators.required],
      description:['', Validators.required],
      isDeleted:['', Validators.required],
    });
   }

  ngOnInit(): void {
  }

  get name(){
    return this.categoryForm.get('name');
  }

  get urlStug(){
    return this.categoryForm.get('urlStug');
  }

  get description(){
    return this.categoryForm.get('description');
  }

  get isDeleted(){
    return this.categoryForm.get('isDeleted');
  }

  getCategoryById() {
    this.categoryService.getCategoryById(this.id).subscribe(res => {
      if (res.status == 200) {
        this.category = res.body;
        this.categoryForm.controls['id'].setValue(this.category.id);
        this.categoryForm.controls['name'].setValue(this.category.name);
        this.categoryForm.controls['urlStug'].setValue(this.category.urlStug);
        this.categoryForm.controls['description'].setValue(this.category.description);
        this.categoryForm.controls['isDeleted'].setValue(this.category.isDeleted);
      }
      else {
        console.log('get category by id failed!');
      }

    })
  }

  Update() {
    if(!this.categoryForm.invalid){
      this.categoryService.updateCategory(this.categoryForm.value).subscribe(res=>{
        if(res.status==200){
          this.noticeService.show('success', 'Thanh cong');
          this.router.navigateByUrl('/category');
        }
        else{
          alert('Failed');
        }
       })
    }
    else{
      alert('Validate form error');
    }

  }

}
