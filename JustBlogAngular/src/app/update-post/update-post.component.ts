import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { CategoryService } from '../shared/category.service';
import { PostService } from '../shared/post.service';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';

@Component({
  selector: 'app-update-post',
  templateUrl: './update-post.component.html',
  styleUrls: ['./update-post.component.css']
})
export class UpdatePostComponent implements OnInit {

  post: any;
  id:any;
  postForm: FormGroup;
  categories:any;
  Editor:any;
  constructor(private route: ActivatedRoute, private postService: PostService, private fb: FormBuilder,private router:Router, private categoryService:CategoryService) {
    this.id = this.route.snapshot.paramMap.get('id');
    this.getPostById();
    this.postForm = this.fb.group({
      id:"",
      title:['', Validators.required],
      shortDescription:['', Validators.required],
      urlSlug:['', Validators.required],
      isDeleted:['', Validators.required],
      tags:['', Validators.required],
      categoryId:['', Validators.required],
      postContent:['', Validators.required],
    });
    this.Editor = ClassicEditor;
   }

   ngOnInit(): void {
    this.getCategories();
  }

  getCategories(){
    this.categoryService.getCategories().subscribe(res=>{
      console.log(res);
     if(res.status==200){
       this.categories=res.body;
     }
     else{
       alert('Get data from serve  failed');
     }
    })
  }


  update() {
    if(!this.postForm.invalid){
      this.postService.updatePost(this.postForm.value).subscribe(res=>{
        if(res.status==200){
          alert('update is successfully');
          this.router.navigateByUrl('/post');
        }
        else{
          alert('Failed');
        }
       })
    }
    else{
      alert('Validate form error');
    }

  }

  getPostById() {
    this.postService.getPostById(this.id).subscribe(res => {
      if (res.status == 200) {
        this.post = res.body;
        this.postForm.controls['id'].setValue(this.post.id);
        this.postForm.controls['title'].setValue(this.post.title);
        this.postForm.controls['shortDescription'].setValue(this.post.shortDescription);
        this.postForm.controls['urlSlug'].setValue(this.post.urlSlug);
        this.postForm.controls['isDeleted'].setValue(this.post.isDeleted);
        this.postForm.controls['tags'].setValue(this.post.tags);
        this.postForm.controls['categoryId'].setValue(this.post.categoryId);
        this.postForm.controls['postContent'].setValue(this.post.postContent);
      }
      else {
        console.log('get category by id failed!');
      }

    })
  }
  get title(){
    return this.postForm.get('title');
  }

  get shortDescription(){
    return this.postForm.get('shortDescription');
  }

  get urlSlug(){
    return this.postForm.get('urlSlug');
  }

  get tags(){
    return this.postForm.get('tags');
  }

  get categoryId(){
    return this.postForm.get('categoryId');
  }

  get postContent(){
    return this.postForm.get('postContent');
  }

  get isDeleted(){
    return this.postForm.get('isDeleted');
  }

}
