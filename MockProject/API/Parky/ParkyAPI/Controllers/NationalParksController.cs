﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ParkyAPI.Models;
using ParkyAPI.Models.Dtos;
using ParkyAPI.Repository.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ParkyAPI.Controllers
{
    [Route("api/v{version:apiVersion}/nationalparks")]
    //[Route("api/[controller]")]
    [ApiController]
    //[ApiExplorerSettings(GroupName = "NationalPark")]
    public class NationalParksController : ControllerBase
    {
        private readonly INationalParkRepository nationalParkRepository;
        private readonly IMapper mapper;

        public NationalParksController(INationalParkRepository nationalParkRepository, IMapper mapper)
        {
            this.nationalParkRepository = nationalParkRepository;
            this.mapper = mapper;
        }

        /// <summary>
        /// Get all National Parks
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(200, Type = typeof(List<NationalParkDto>))]
        [ProducesResponseType(400)]
        public IActionResult GetNationalParks()
        {
            var nationalParks = nationalParkRepository.GetNationalParks();
            var nationalParkDtos = new List<NationalParkDto>();

            foreach (var item in nationalParks)
                nationalParkDtos.Add(mapper.Map<NationalParkDto>(item));

            return Ok(nationalParkDtos);
        }

        /// <summary>
        /// Get National Park By Id
        /// </summary>
        /// <param name="id">Id of National Park</param>
        /// <returns></returns>
        [HttpGet("{id:int}", Name = "GetNationalPark")]
        [Authorize]
        public IActionResult GetNationalPark(int id)
        {
            var nationalPark = nationalParkRepository.GetNationalPark(id);

            if (nationalPark == null)
                return NotFound();

            var nationalParkDto = mapper.Map<NationalParkDto>(nationalPark);
            return Ok(nationalParkDto);
        }

        [HttpPost]
        public IActionResult CreateNationalPark([FromBody] NationalParkDto nationalParkDto)
        {
            if (nationalParkDto == null)
                return BadRequest(ModelState);

            if(nationalParkRepository.NationParkExists(nationalParkDto.Name))
            {
                ModelState.AddModelError("", "Name is Exists");
                return StatusCode(404, ModelState);
            }

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var nationalPark = mapper.Map<NationalPark>(nationalParkDto);

            if (!nationalParkRepository.CreateNationalPark(nationalPark))
            {
                ModelState.AddModelError("", "Error");
                return StatusCode(500, ModelState);
            }

            return CreatedAtRoute("GetNationalPark", new {version = HttpContext.GetRequestedApiVersion().ToString(),
                id = nationalPark.Id }, nationalPark);
        }

        [HttpPatch("{id:int}", Name = "UpdateNationalPark")]
        public IActionResult UpdateNationalPark(int id, [FromBody] NationalParkDto nationalParkDto)
        {
            if (nationalParkDto == null || id != nationalParkDto.Id)
                return BadRequest(ModelState);

            if (nationalParkRepository.NationParkExists(nationalParkDto.Name))
            {
                ModelState.AddModelError("", "Name is Exists");
                return StatusCode(404, ModelState);
            }

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var nationalPark = mapper.Map<NationalPark>(nationalParkDto);

            if (!nationalParkRepository.UpdateNationalPark(nationalPark))
            {
                ModelState.AddModelError("", "Error");
                return StatusCode(500, ModelState);
            }

            return NoContent();
        }

        [HttpDelete("{id:int}", Name = "DeleteNationalPark")]
        public IActionResult DeleteNationalPark(int id)
        {
            if (!nationalParkRepository.NationParkExists(id))
                return NotFound();

            var nationalPark = nationalParkRepository.GetNationalPark(id);

            if (!nationalParkRepository.DeleteNationalPark(nationalPark))
            {
                ModelState.AddModelError("", "Error");
                return StatusCode(500, ModelState);
            }

            return NoContent();
        }
    }
}
