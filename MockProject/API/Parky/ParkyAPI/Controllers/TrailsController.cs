﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ParkyAPI.Models;
using ParkyAPI.Models.Dtos;
using ParkyAPI.Repository.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ParkyAPI.Controllers
{
    //[Route("api/[controller]")]
    [Route("api/v{version:apiVersion}/trails")]
    [ApiController]
    //[ApiExplorerSettings(GroupName = "Trail")]
    public class TrailsController : ControllerBase
    {
        private readonly ITrailRepository trailRepository;
        private readonly IMapper mapper;

        public TrailsController(ITrailRepository trailRepository, IMapper mapper)
        {
            this.trailRepository = trailRepository;
            this.mapper = mapper;
        }

        /// <summary>
        /// Get all Trails
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(200, Type = typeof(List<TrailDto>))]
        [ProducesResponseType(400)]
        public IActionResult GetTrails()
        {
            var trails = trailRepository.GetTrails();
            var trailDtos = new List<TrailDto>();

            foreach (var item in trails)
                trailDtos.Add(mapper.Map<TrailDto>(item));

            return Ok(trailDtos);
        }

        /// <summary>
        /// Get Trails  By Id
        /// </summary>
        /// <param name="id">Id of Trail</param>
        /// <returns></returns>
        [HttpGet("{id:int}", Name = "GetTrail")]
        [Authorize(Roles ="admin")]
        public IActionResult GetTrail(int id)
        {
            var trail = trailRepository.GetTrail(id);

            if (trail == null)
                return NotFound();

            var trailDto = mapper.Map<TrailDto>(trail);
            return Ok(trailDto);
        }

        [HttpGet("[action]/{nationalParkId:int}")]
        public IActionResult GetTrailInNationalPark(int nationalParkId)
        {
            var trails = trailRepository.GetTrailsInNationalPark(nationalParkId);

            if (trails == null)
                return NotFound();

            var trailDtos = new List<TrailDto>();
            foreach (var item in trails)
            {
                trailDtos.Add(mapper.Map<TrailDto>(item));
            }
            return Ok(trailDtos);
        }

        [HttpPost]
        public IActionResult CreateTrail([FromBody] TrailUpsetDto trailDto)
        {
            if (trailDto == null)
                return BadRequest(ModelState);

            if (trailRepository.TrailExists(trailDto.Name))
            {
                ModelState.AddModelError("", "Name is Exists");
                return StatusCode(404, ModelState);
            }

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var trail = mapper.Map<Trail>(trailDto);

            if (!trailRepository.CreateTrail(trail))
            {
                ModelState.AddModelError("", "Error");
                return StatusCode(500, ModelState);
            }

            return CreatedAtRoute("GetTrails", new { id = trail.Id }, trail);
        }

        [HttpPatch("{id:int}", Name = "UpdateTrail")]
        public IActionResult UpdateTrail(int id, [FromBody] TrailUpsetDto trailDto)
        {
            if (trailDto == null || id != trailDto.Id)
                return BadRequest(ModelState);

            if (trailRepository.TrailExists(trailDto.Name))
            {
                ModelState.AddModelError("", "Name is Exists");
                return StatusCode(404, ModelState);
            }

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var trail = mapper.Map<Trail>(trailDto);

            if (!trailRepository.UpdateTrail(trail))
            {
                ModelState.AddModelError("", "Error");
                return StatusCode(500, ModelState);
            }

            return NoContent();
        }

        [HttpDelete("{id:int}", Name = "DeleteTrail")]
        public IActionResult DeleteTrail(int id)
        {
            if (!trailRepository.TrailExists(id))
                return NotFound();

            var trail = trailRepository.GetTrail(id);

            if (!trailRepository.DeleteTrail(trail))
            {
                ModelState.AddModelError("", "Error");
                return StatusCode(500, ModelState);
            }

            return NoContent();
        }
    }
}
