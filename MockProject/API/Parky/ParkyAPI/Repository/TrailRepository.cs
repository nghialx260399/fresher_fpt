﻿using Microsoft.EntityFrameworkCore;
using ParkyAPI.Data;
using ParkyAPI.Models;
using ParkyAPI.Repository.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ParkyAPI.Repository
{
    public class TrailRepository : ITrailRepository
    {
        private readonly ApplicationDbContext context;

        public TrailRepository(ApplicationDbContext context)
        {
            this.context = context;
        }
        public bool CreateTrail(Trail trail)
        {
            context.Trails.Add(trail);
            return Save();
        }

        public bool DeleteTrail(Trail trail)
        {
            context.Trails.Remove(trail);
            return Save();
        }

        public Trail GetTrail(int trailId)
        {
            return context.Trails.Include(x => x.NationalPark).FirstOrDefault(x => x.Id == trailId);
        }

        public ICollection<Trail> GetTrails()
        {
            return context.Trails.Include(x => x.NationalPark).ToList();
        }

        public ICollection<Trail> GetTrailsInNationalPark(int nationalParkId)
        {
            return context.Trails.Include(x => x.NationalPark).Where(x => x.NationalParkId == nationalParkId).ToList();
        }

        public bool Save()
        {
            return context.SaveChanges() > 0;
        }

        public bool TrailExists(string name)
        {
            return context.Trails.Any(x => x.Name.ToLower().Trim() == name.ToLower().Trim());
        }

        public bool TrailExists(int id)
        {
            return context.Trails.Any(x => x.Id == id);
        }

        public bool UpdateTrail(Trail trail)
        {
            context.Trails.Update(trail);
            return Save();
        }
    }
}
