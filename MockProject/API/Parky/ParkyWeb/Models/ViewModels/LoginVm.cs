﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ParkyWeb.Models.ViewModels
{
    public class LoginVm
    {
        public string Username { get; set; }

        public string Password { get; set; }
    }
}
