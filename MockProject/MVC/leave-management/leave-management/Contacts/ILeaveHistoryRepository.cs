﻿using leave_management.Data;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace leave_management.Contacts
{
    public interface ILeaveHistoryRepository : IRepositoryBase<LeaveRequests>
    {
        Task<ICollection<LeaveRequests>> GetLeaveRequestsByEmployee(string employeeId);
    }
}