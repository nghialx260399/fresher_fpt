﻿using leave_management.Data;
using System;
using System.Threading.Tasks;

namespace leave_management.Contacts
{
    public interface IUnitOfWork : IDisposable
    {
        IGenericRepository<LeaveType> LeaveTypes { get; }

        IGenericRepository<LeaveRequests> LeaveRequests { get; }

        IGenericRepository<LeaveAllocation> LeaveAllocations { get; }

        IGenericRepository<Employee> Employees { get; }

        Task Save();
    }
}