﻿using AutoMapper;
using leave_management.Contacts;
using leave_management.Data;
using leave_management.Models;
using leave_management.Service.LeaveAllService;
using leave_management.Service.LeaveTypes;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace leave_management.Controllers
{
    [Authorize(Roles ="Admin")]
    public class LeaveAllocationsController : Controller
    {
        private readonly ILeaveTypeRepository leaveTypeRepository;
        private readonly ILeaveAllocationRepository leaveAllocationRepository;
        private readonly IMapper mapper;
        private readonly UserManager<Employee> userManager;
        private readonly IUnitOfWork unitOfWork;
        private readonly ILeaveTypeService leaveTypeService;
        private readonly ILeaveAllService leaveAllService;

        public LeaveAllocationsController(ILeaveTypeRepository leaveTypeRepository, ILeaveAllocationRepository leaveAllocationRepository,
            IMapper mapper, UserManager<Employee> userManager, IUnitOfWork unitOfWork,
            ILeaveTypeService leaveTypeService, ILeaveAllService leaveAllService)
        {
            this.leaveTypeRepository = leaveTypeRepository;
            this.leaveAllocationRepository = leaveAllocationRepository;
            this.mapper = mapper;
            this.userManager = userManager;
            this.unitOfWork = unitOfWork;
            this.leaveTypeService = leaveTypeService;
            this.leaveAllService = leaveAllService;
        }

        // GET: LeaveAllocationsController
        //public async Task<ActionResult> Index()
        //{
        //    if (TempData["InfoAllocate"] != null)
        //        TempData["Info"] = TempData["InfoAllocate"];

        //    var leaveTypes = await unitOfWork.LeaveTypes.FindAll();
        //    var mappedLeaveTypes = mapper.Map<List<DetailsLeaveTypeVM>>(leaveTypes.ToList());
        //    var model = new CreateLeaveAllocationVM
        //    {
        //        LeaveTypes = mappedLeaveTypes,
        //        NumberUpdated = 0
        //    };

        //    return View(model);
        //}

        public async Task<ActionResult> Index(string keyword = null, int pageIndex = 1, int pageSize = 5)
        {
            if (TempData["InfoAllocate"] != null)
                TempData["Info"] = TempData["InfoAllocate"];

            Expression<Func<LeaveType, bool>> filter = null;
            if (!string.IsNullOrEmpty(keyword))
            {
                ViewBag.Keyword = keyword;
                keyword = keyword.ToLower();
                filter = x => x.Name.ToLower().Contains(keyword.ToLower());
            }
            var leaveTypePaged = await this.leaveTypeService.GetPaging(filter: filter, pageIndex: pageIndex, pageSize: pageSize);

            return View(leaveTypePaged);
        }

        public async Task<ActionResult> SetLeave(int id)
        {
            var leaveType = await unitOfWork.LeaveTypes.Find(q => q.Id == id);
            var employees = await userManager.GetUsersInRoleAsync("Employee");
            var period = DateTime.Now.Year;

            foreach (var item in employees)
            {
                if (await unitOfWork.LeaveAllocations.IsExists(q => q.EmployeeId == item.Id
                                        && q.LeaveTypeId == id
                                        && q.Period == period))
                    continue;
                var allocation = new LeaveAllocationVM
                {
                    DateCreated = DateTime.Now,
                    EmployeeId = item.Id,
                    LeaveTypeId = id,
                    NumberOfDays = leaveType.DefaultDays,
                    Period = DateTime.Now.Year
                };

                var leaveAllocation = mapper.Map<LeaveAllocation>(allocation);

                await unitOfWork.LeaveAllocations.Create(leaveAllocation);
                await unitOfWork.Save();

                TempData["InfoAllocate"] = "Success";
            }

            return RedirectToAction(nameof(Index));
        }

        //public async Task<ActionResult> ListEmployees()
        //{
        //    var employees = await userManager.GetUsersInRoleAsync("Employee");
        //    var model = mapper.Map<List<EmployeeVM>>(employees);

        //    return View(model);
        //}

        public async Task<ActionResult> ListEmployees(string keyword = null, int pageIndex = 1, int pageSize = 5)
        {
            Expression<Func<Employee, bool>> filter = null;
            if (!string.IsNullOrEmpty(keyword))
            {
                ViewBag.Keyword = keyword;
                keyword = keyword.ToLower();
                filter = x => x.FirstName.ToLower().Contains(keyword);
            }
            var employeePaged = await this.leaveAllService.GetPaging(filter: filter, pageIndex: pageIndex, pageSize: pageSize);

            return View(employeePaged);
        }

        // GET: LeaveAllocationsController/Details/5
        public async Task<ActionResult> Details(string id)
        {
            if (TempData["InfoUpdate"] != null)
                TempData["Info"] = TempData["InfoUpdate"];

            var employee = mapper.Map<EmployeeVM>(await userManager.FindByIdAsync(id));
            var period = DateTime.Now.Year;

            var records = await unitOfWork.LeaveAllocations.FindAll(
                expression: q => q.EmployeeId == id && q.Period == period,
                includes: q => q.Include(x => x.LeaveType)
                );

            var allocations = mapper.Map<List<LeaveAllocationVM>>
                    (records);

            var model = new ViewAllocationVM
            {
                Employee = employee,
                LeaveAllocations = allocations
            };
            return View(model);
        }

        // GET: LeaveAllocationsController/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: LeaveAllocationsController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: LeaveAllocationsController/Edit/5
        public async Task<ActionResult> Edit(int id)
        {
            var leaveAllocation = await unitOfWork.LeaveAllocations.Find(q => q.Id == id,
                includes: q => q.Include(x => x.Employee).Include(x => x.LeaveType));
            var model = mapper.Map<EditLeaveAllocationVM>(leaveAllocation);

            return View(model);
        }

        // POST: LeaveAllocationsController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(EditLeaveAllocationVM model)
        {
            try
            {
                if (!ModelState.IsValid)
                    return View(model);

                var record = await unitOfWork.LeaveAllocations.Find(q => q.Id == model.Id);
                record.NumberOfDays = model.NumberOfDays;

                unitOfWork.LeaveAllocations.Update(record);
                await unitOfWork.Save();
                TempData["InfoUpdate"] = "Update Success";

                return RedirectToAction(nameof(Details), new { id = model.EmployeeId });
            }
            catch
            {
                return View();
            }
        }

        // GET: LeaveAllocationsController/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: LeaveAllocationsController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}
