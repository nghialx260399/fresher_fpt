﻿using AutoMapper;
using leave_management.Contacts;
using leave_management.Data;
using leave_management.Models;
using leave_management.Service;
using leave_management.Service.LeaveRequestService;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace leave_management.Controllers
{
    [Authorize]
    public class LeaveRequestsController : Controller
    {
        private readonly IMapper mapper;
        private readonly ILeaveHistoryRepository leaveHistoryRepository;
        private readonly UserManager<Employee> userManager;
        private readonly ILeaveAllocationRepository leaveAllocationRepository;
        private readonly ILeaveTypeRepository leaveTypeRepository;
        private readonly IUnitOfWork unitOfWork;
        private readonly IEmailSender emailSender;
        private readonly ILeaveRequestService leaveRequestService;

        public LeaveRequestsController(IMapper mapper, ILeaveHistoryRepository leaveHistoryRepository,
            UserManager<Employee> userManager, ILeaveAllocationRepository leaveAllocationRepository,
            ILeaveTypeRepository leaveTypeRepository, IUnitOfWork unitOfWork, IEmailSender emailSender,
            ILeaveRequestService leaveRequestService)
        {
            this.mapper = mapper;
            this.leaveHistoryRepository = leaveHistoryRepository;
            this.userManager = userManager;
            this.leaveAllocationRepository = leaveAllocationRepository;
            this.leaveTypeRepository = leaveTypeRepository;
            this.unitOfWork = unitOfWork;
            this.emailSender = emailSender;
            this.leaveRequestService = leaveRequestService;
        }
        // GET: LeaveRequestsController
        //[Authorize(Roles ="Admin")]
        //public async Task<ActionResult> Index()
        //{
        //    if (TempData["InfoReject"] != null)
        //        TempData["Info"] = TempData["InfoReject"];

        //    if (TempData["InfoApp"] != null)
        //        TempData["Info"] = TempData["InfoApp"];

        //    var leaveRequests = await unitOfWork.LeaveRequests.FindAll(
        //        includes: q => q.Include(x => x.RequestingEmployee).Include(x => x.LeaveType));
        //    var leaveRequstsModel = mapper.Map<List<LeaveRequestVM>>(leaveRequests);
        //    var model = new AdminLeaveRequestViewVM
        //    {
        //        TotalRequests = leaveRequstsModel.Count,
        //        ApprovedRequests = leaveRequstsModel.Count(q => q.Approved == true),
        //        PendingRequests = leaveRequstsModel.Count(q => q.Approved == null),
        //        RejectedRequests = leaveRequstsModel.Count(q => q.Approved == false),
        //        LeaveRequests = leaveRequstsModel
        //    };

        //    return View(model);
        //}

        [Authorize(Roles = "Admin")]
        public async Task<ActionResult> Index(string keyword = null, int pageIndex = 1, int pageSize = 5)
        {
            if (TempData["InfoReject"] != null)
                TempData["Info"] = TempData["InfoReject"];

            if (TempData["InfoApp"] != null)
                TempData["Info"] = TempData["InfoApp"];

            Expression<Func<LeaveRequests, bool>> filter = null;
            if (!string.IsNullOrEmpty(keyword))
            {
                ViewBag.Keyword = keyword;
                keyword = keyword.ToLower();
                filter = x => x.RequestingEmployee.FirstName.ToLower().Contains(keyword.ToLower());
            }
            var leaveRequestPaged = await this.leaveRequestService.GetPagingRequestAdmin(filter: filter, pageIndex: pageIndex, pageSize: pageSize);
  
            var model = new AdminLeaveRequestViewVM
            {
                TotalRequests = leaveRequestPaged.Data.Count(),
                ApprovedRequests = leaveRequestPaged.Data.Count(q => q.Approved == true),
                PendingRequests = leaveRequestPaged.Data.Count(q => q.Approved == null),
                RejectedRequests = leaveRequestPaged.Data.Count(q => q.Approved == false),
                LeaveRequests = leaveRequestPaged.Data.ToList(),
                ToTalPage = leaveRequestPaged.TotalPage,
                PageIndex = leaveRequestPaged.PageIndex,
            };

            return View(model);
        }

        // GET: LeaveRequestsController/Details/5
        [Authorize(Roles = "Admin")]
        public async Task<ActionResult> Details(int id)
        {
            var leaveRequest = await unitOfWork.LeaveRequests.Find(x => x.Id == id,
                includes: q => q.Include(x => x.RequestingEmployee).Include(x => x.LeaveType).Include(x => x.ApprovedBy));
            var model = mapper.Map<LeaveRequestVM>(leaveRequest);

            return View(model);
        }

        //public async Task<ActionResult> MyLeave()
        //{
        //    if (TempData["InfoCreate"] != null)
        //        TempData["Info"] = TempData["InfoCreate"];

        //    var employee = await userManager.GetUserAsync(User);
        //    var employeeId = employee.Id;
        //    var employeeAllocations = await unitOfWork.LeaveAllocations.FindAll(x => x.EmployeeId == employeeId,
        //        includes: q => q.Include(x => x.LeaveType));
        //    var employeeRequests = await unitOfWork.LeaveRequests.FindAll(x => x.RequestingEmployeeId == employeeId);

        //    var employeeAllocationsModel = mapper.Map<List<LeaveAllocationVM>>(employeeAllocations);
        //    var employeeRequestsModel = mapper.Map<List<LeaveRequestVM>>(employeeRequests);

        //    var model = new EmployeeLeaveRequestViewVM
        //    {
        //        LeaveAllocations = employeeAllocationsModel,
        //        LeaveRequests = employeeRequestsModel
        //    };

        //    return View(model);

        //}

        public async Task<ActionResult> MyLeave(string keyword = null, int pageIndex = 1, int pageSize = 5)
        {
            if (TempData["InfoCreate"] != null)
                TempData["Info"] = TempData["InfoCreate"];

            var employee = await userManager.GetUserAsync(User);
            var employeeId = employee.Id;
            var employeeAllocations = await  unitOfWork.LeaveAllocations.FindAll(x => x.EmployeeId == employeeId,
                includes: q => q.Include(x => x.LeaveType));

            Expression<Func<LeaveRequests, bool>> filter = null;

            filter = x => x.RequestingEmployeeId == employeeId;
            if (!string.IsNullOrEmpty(keyword))
            {
                ViewBag.Keyword = keyword;
                keyword = keyword.ToLower();
                filter = x => x.RequestingEmployeeId == employeeId + "&&" + x.LeaveType.Name.ToLower().Contains(keyword.ToLower());
            }
            var leaveRequestPaged = await this.leaveRequestService.GetPagingRequest(filter: filter, pageIndex: pageIndex, pageSize: pageSize);

            var employeeAllocationsModel = mapper.Map<List<LeaveAllocationVM>>(employeeAllocations);

            var model = new EmployeeLeaveRequestViewVM
            {
                LeaveAllocations = employeeAllocationsModel,
                LeaveRequests = leaveRequestPaged.Data.ToList(),
                TotalPage = leaveRequestPaged.TotalPage,
                PageIndex= leaveRequestPaged.PageIndex
            };

            return View(model);

        }

        // GET: LeaveRequestsController/Create
        [Authorize(Roles = "Admin")]
        public async Task<ActionResult> ApproveRequest(int id)
        {
            try
            {
                var user = await userManager.GetUserAsync(User);
                var leaveRequest = await unitOfWork.LeaveRequests.Find(x => x.Id == id);
                var employeeId = leaveRequest.RequestingEmployeeId;
                var leaveTypeId = leaveRequest.LeaveTypeId;
                var period = DateTime.Now.Year;
                var allocation = await unitOfWork.LeaveAllocations.Find(x => x.EmployeeId == employeeId
                                                                        && x.Period == period
                                                                        && x.LeaveTypeId == leaveTypeId);
                int daysRequested = (int)(leaveRequest.EndDate - leaveRequest.StartDate).TotalDays;

                allocation.NumberOfDays = allocation.NumberOfDays - daysRequested;

                leaveRequest.Approved = true;
                leaveRequest.ApprovedById = user.Id;
                leaveRequest.DateActioned = DateTime.Now;

                unitOfWork.LeaveRequests.Update(leaveRequest);
                unitOfWork.LeaveAllocations.Update(allocation);
                await unitOfWork.Save();
                TempData["InfoApp"] = "Approve Success";

                return RedirectToAction(nameof(Index));

            }
            catch (Exception ex)
            {
                return RedirectToAction(nameof(Index));
            }
        }

        [Authorize(Roles = "Admin")]
        public async Task<ActionResult> RejectRequest(int id)
        {
            try
            {
                var user = await userManager.GetUserAsync(User);
                var leaveRequest = await unitOfWork.LeaveRequests.Find(x => x.Id == id);
                leaveRequest.Approved = false;
                leaveRequest.ApprovedById = user.Id;
                leaveRequest.DateActioned = DateTime.Now;

                unitOfWork.LeaveRequests.Update(leaveRequest);
                await unitOfWork.Save();
                TempData["InfoReject"] = "Reject Success";
                return RedirectToAction(nameof(Index));

            }
            catch (Exception ex)
            {
                return RedirectToAction(nameof(Index));
            }
        }

        // GET: LeaveRequestsController/Delete/5
        // GET: LeaveRequest/Create
        public async Task<ActionResult> Create()
        {
            var leaveTypes = await unitOfWork.LeaveTypes.FindAll();
            var leaveTypeItems = leaveTypes.Select(q => new SelectListItem
            {
                Text = q.Name,
                Value = q.Id.ToString()
            });
            var model = new CreateLeaveRequestVM
            {
                LeaveTypes = leaveTypeItems
            };
            return View(model);
        }

        // POST: LeaveRequest/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(CreateLeaveRequestVM model)
        {

            try
            {
                var startDate = Convert.ToDateTime(model.StartDate);
                var endDate = Convert.ToDateTime(model.EndDate);
                var leaveTypes = await unitOfWork.LeaveTypes.FindAll();
                var employee = await userManager.GetUserAsync(User);
                var period = DateTime.Now.Year;
                var allocation = await unitOfWork.LeaveAllocations.Find(x => x.EmployeeId == employee.Id
                                                                        && x.Period == period
                                                                        && x.LeaveTypeId == model.LeaveTypeId);

                var leaveTypeItems = leaveTypes.Select(q => new SelectListItem
                {
                    Text = q.Name,
                    Value = q.Id.ToString()
                });
                model.LeaveTypes = leaveTypeItems;

                if (!ModelState.IsValid)
                {
                    return View(model);
                }

                if (DateTime.Compare(startDate, endDate) > 1)
                {
                    ModelState.AddModelError("", "Start Date cannot be further in the future than the End Date");
                    return View(model);
                }

                int daysRequested = (int)(endDate - startDate).TotalDays;

                if (daysRequested > allocation.NumberOfDays)
                {
                    ModelState.AddModelError("", $"The day requesting must be less than {allocation.NumberOfDays}");
                    return View(model);
                }

                var leaveRequestModel = new LeaveRequestVM
                {
                    RequestingEmployeeId = employee.Id,
                    StartDate = startDate,
                    EndDate = endDate,
                    Approved = null,
                    DateRequested = DateTime.Now,
                    DateActioned = DateTime.Now,
                    LeaveTypeId = model.LeaveTypeId,
                    RequestComments = model.RequestComments
                };

                var leaveRequest = mapper.Map<LeaveRequests>(leaveRequestModel);

                await unitOfWork.LeaveRequests.Create(leaveRequest);
                await unitOfWork.Save();

                TempData["InfoCreate"] = "Success";
                //await emailSender.SendEmailAsync("admin@gmail.com", "New Leave Request",
                //    $"Please review this leave request. <a href='UrlOfApp/{leaveRequest.Id}'>Click Here</a>");

                return RedirectToAction("MyLeave");
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", "This Leave Type can't use");
                return View(model);
            }
        }

        public async Task<ActionResult> CancelRequest(int id)
        {
            var leaveRequest = await unitOfWork.LeaveRequests.Find(x => x.Id == id);
            leaveRequest.Cancelled = true;

            unitOfWork.LeaveRequests.Update(leaveRequest);
            await unitOfWork.Save();

            return RedirectToAction("MyLeave");
        }

        // GET: LeaveRequest/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: LeaveRequest/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: LeaveRequest/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: LeaveRequest/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}
