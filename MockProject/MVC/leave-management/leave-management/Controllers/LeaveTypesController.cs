﻿using AutoMapper;
using leave_management.Contacts;
using leave_management.Data;
using leave_management.Models;
using leave_management.Service.LeaveTypes;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace leave_management.Controllers
{
    [Authorize(Roles = "Admin")]
    public class LeaveTypesController : Controller
    {
        private readonly ILeaveTypeRepository leaveTypeRepository;
        private readonly IMapper mapper;
        private readonly IUnitOfWork unitOfWork;
        private readonly ILeaveTypeService leaveTypeService;

        public LeaveTypesController(ILeaveTypeRepository leaveTypeRepository, IMapper mapper, IUnitOfWork unitOfWork,
            ILeaveTypeService leaveTypeService)
        {
            this.leaveTypeRepository = leaveTypeRepository;
            this.mapper = mapper;
            this.unitOfWork = unitOfWork;
            this.leaveTypeService = leaveTypeService;
        }

        // GET: LeaveTypesController
        //public async Task<ActionResult> Index()
        //{
        //    if (TempData["InfoCreate"] != null)
        //        TempData["Info"] = TempData["InfoCreate"];

        //    if (TempData["InfoUpdate"] != null)
        //        TempData["Info"] = TempData["InfoUpdate"];

        //    if(TempData["InfoDelete"] != null)
        //        TempData["Info"] = TempData["InfoDelete"];

        //    var leaveTypes = await unitOfWork.LeaveTypes.FindAll();
        //    var model = mapper.Map<List<DetailsLeaveTypeVM>>(leaveTypes.ToList());

        //    return View(model);
        //}

        // GET: LeaveTypesController
        public async Task<ActionResult> Index(string keyword = null, int pageIndex = 1, int pageSize = 5)
        {
            if (TempData["InfoCreate"] != null)
                TempData["Info"] = TempData["InfoCreate"];

            if (TempData["InfoUpdate"] != null)
                TempData["Info"] = TempData["InfoUpdate"];

            if (TempData["InfoDelete"] != null)
                TempData["Info"] = TempData["InfoDelete"];

            Expression<Func<LeaveType, bool>> filter = null;
            if (!string.IsNullOrEmpty(keyword))
            {
                ViewBag.Keyword = keyword;
                keyword = keyword.ToLower();
                filter = x => x.Name.ToLower().Contains(keyword.ToLower());
            }
            var leaveTypePaged = await this.leaveTypeService.GetPaging(filter: filter, pageIndex:pageIndex, pageSize:pageSize);

            return View(leaveTypePaged);
        }

        // GET: LeaveTypesController/Details/5

        public async Task<ActionResult> Details(int id)
        {
            var isExists = await unitOfWork.LeaveTypes.IsExists(x => x.Id == id);

            if (!isExists)
                return NotFound();

            var leaveType = await unitOfWork.LeaveTypes.Find(x => x.Id == id);
            var model = mapper.Map<DetailsLeaveTypeVM>(leaveType);

            return View(model);
        }

        // GET: LeaveTypesController/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: LeaveTypesController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(DetailsLeaveTypeVM model)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    ModelState.AddModelError("", "Name and DefaultDays is not required");
                    return View(model);
                }

                var leaveType = mapper.Map<LeaveType>(model);
                leaveType.DateCreated = DateTime.Now;

                await unitOfWork.LeaveTypes.Create(leaveType);

                await unitOfWork.Save();
                TempData["InfoCreate"] = "Create Success";

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                ModelState.AddModelError("", "Error");
                return View(model);
            }
        }

        // GET: LeaveTypesController/Edit/5
        public async Task<ActionResult> Edit(int id)
        {
            if (!await unitOfWork.LeaveTypes.IsExists(x => x.Id == id))
                return NotFound();

            var leaveType = await unitOfWork.LeaveTypes.Find(x => x.Id == id);
            var model = mapper.Map<DetailsLeaveTypeVM>(leaveType);

            return View(model);
        }

        // POST: LeaveTypesController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(DetailsLeaveTypeVM model)
        {
            try
            {
                if (!ModelState.IsValid)
                    return View(model);

                var leaveType = mapper.Map<LeaveType>(model);

                unitOfWork.LeaveTypes.Update(leaveType);
                await unitOfWork.Save();
                TempData["InfoUpdate"] = "Update Success";
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                ModelState.AddModelError("", "Error");
                return View(model);
            }
        }

        // GET: LeaveTypesController/Delete/5
        public async Task<ActionResult> Delete(int id)
        {
            var leaveType = await unitOfWork.LeaveTypes.Find(x => x.Id == id);

            if (leaveType == null)
                return NotFound();

            unitOfWork.LeaveTypes.Delete(leaveType);
            await unitOfWork.Save();
            TempData["InfoDelete"] = "Delete Success";

            return RedirectToAction(nameof(Index));
        }

        // POST: LeaveTypesController/Delete/5
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Delete(int id, DetailsLeaveTypeVM model)
        //{
        //    try
        //    {
        //        var leaveType = leaveTypeRepository.FindById(id);

        //        if (leaveType == null)
        //            return NotFound();

        //        var isSuccess = leaveTypeRepository.Delete(leaveType);

        //        if (!isSuccess)
        //        {
        //            ModelState.AddModelError("", "Error");
        //            return View(model);
        //        }

        //        return RedirectToAction(nameof(Index));
        //    }
        //    catch
        //    {
        //        ModelState.AddModelError("", "Error");
        //        return View(model);
        //    }
        //}
    }
}
