﻿using System.Collections.Generic;

namespace leave_management.Models
{
    public class CreateLeaveAllocationVM
    {
        public int NumberUpdated { get; set; }

        public List<DetailsLeaveTypeVM> LeaveTypes { get; set; }
    }
}