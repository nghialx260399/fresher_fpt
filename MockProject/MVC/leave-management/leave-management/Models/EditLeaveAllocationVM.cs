﻿namespace leave_management.Models
{
    public class EditLeaveAllocationVM
    {
        public int NumberOfDays { get; set; }

        public int Id { get; set; }

        public EmployeeVM Employee { get; set; }

        public string EmployeeId { get; set; }

        public DetailsLeaveTypeVM LeaveTypeVM { get; set; }
    }
}