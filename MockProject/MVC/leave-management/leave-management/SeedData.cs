﻿using leave_management.Data;
using Microsoft.AspNetCore.Identity;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace leave_management
{
    public static class SeedData
    {
        public static async Task SeedRolesAsync(UserManager<Employee> userManager, RoleManager<IdentityRole> roleManager)
        {
            //Seed Roles
            await roleManager.CreateAsync(new IdentityRole("Admin"));
            await roleManager.CreateAsync(new IdentityRole("Employee"));
        }

        public static async Task SeedSuperAdminAsync(UserManager<Employee> userManager, RoleManager<IdentityRole> roleManager)
        {
            //Seed Default User
            var defaultUser = new Employee
            {
                UserName = "admin@gmail.com",
                Email = "admin@gmail.com",
                EmailConfirmed = true,
                PhoneNumberConfirmed = true,
                NormalizedEmail = "admin@gmail.com",
                NormalizedUserName = "admin@gmail.com",
                FirstName = "Admin",
                LastName = "Admin",
                DateCreated = DateTime.Now,
                DateJoined = DateTime.Now,
                DateOfBirth = DateTime.Parse("01/01/1991")
            };
            if (userManager.Users.All(u => u.Id != defaultUser.Id))
            {
                var user = await userManager.FindByEmailAsync(defaultUser.Email);
                if (user == null)
                {
                    await userManager.CreateAsync(defaultUser, "Abcxyz@123");
                    await userManager.AddToRoleAsync(defaultUser, "Admin");
                    await userManager.AddToRoleAsync(defaultUser, "Employee");
                }
            }
        }
    }
}