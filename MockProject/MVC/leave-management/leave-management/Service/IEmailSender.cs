﻿using System.Threading.Tasks;

namespace leave_management.Service
{
    public interface IEmailSender
    {
        Task SendEmailAsync(string email, string subject, string htmlMessage);
    }
}