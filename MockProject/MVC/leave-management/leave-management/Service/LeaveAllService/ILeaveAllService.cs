﻿using leave_management.Data;
using leave_management.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace leave_management.Service.LeaveAllService
{
    public interface ILeaveAllService
    {
        public Task<PagedVm<EmployeeVM>> GetPaging(Expression<Func<Employee, bool>> filter, int pageSize = 10, int pageIndex = 1);
    }
}
