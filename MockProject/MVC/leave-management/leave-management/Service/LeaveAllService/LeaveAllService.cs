﻿using AutoMapper;
using leave_management.Contacts;
using leave_management.Data;
using leave_management.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace leave_management.Service.LeaveAllService
{
    public class LeaveAllService : ILeaveAllService
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly IMapper mapper;

        public LeaveAllService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
        }
        public async Task<PagedVm<EmployeeVM>> GetPaging(Expression<Func<Employee, bool>> filter, int pageSize = 10, int pageIndex = 1)
        {
            Func<IQueryable<Employee>, IOrderedQueryable<Employee>> order = x => x.OrderBy(x => x.FirstName);
            var employees = await this.unitOfWork.Employees
                      .GetPaging(filter: filter, orderBy: order, pageSize: pageSize, pageIndex: pageIndex);

            var employeeVms = this.mapper.Map<IEnumerable<EmployeeVM>>(employees.Data);

            return new PagedVm<EmployeeVM>(employeeVms, pageIndex, pageSize, employees.TotalPage);
        }
    }
}
