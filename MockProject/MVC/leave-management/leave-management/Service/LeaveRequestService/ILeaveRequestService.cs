﻿using leave_management.Data;
using leave_management.Models;
using System;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace leave_management.Service.LeaveRequestService
{
    public interface ILeaveRequestService
    {
        public Task<PagedVm<LeaveRequestVM>> GetPagingRequestAdmin(Expression<Func<LeaveRequests, bool>> filter, int pageSize = 10, int pageIndex = 1);
        
        public Task<PagedVm<LeaveRequestVM>> GetPagingRequest(Expression<Func<LeaveRequests, bool>> filter, int pageSize = 10, int pageIndex = 1);
    }
}