﻿using AutoMapper;
using leave_management.Contacts;
using leave_management.Data;
using leave_management.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace leave_management.Service.LeaveRequestService
{
    public class LeaveRequestService: ILeaveRequestService
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly IMapper mapper;

        public LeaveRequestService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
        }
        public async Task<PagedVm<LeaveRequestVM>> GetPagingRequestAdmin(Expression<Func<LeaveRequests, bool>> filter, int pageSize = 10, int pageIndex = 1)
        {
            Func<IQueryable<LeaveRequests>, IOrderedQueryable<LeaveRequests>> order = x => x.OrderBy(x => x.Id);
            var leaveRequests = await this.unitOfWork.LeaveRequests
                      .GetPaging(filter: filter, orderBy: order, pageSize: pageSize, pageIndex: pageIndex,
                      includes: q => q.Include(x => x.RequestingEmployee).Include(x => x.LeaveType));

            var leaveQuestVm = this.mapper.Map<IEnumerable<LeaveRequestVM>>(leaveRequests.Data);

            return new PagedVm<LeaveRequestVM>(leaveQuestVm, pageIndex, pageSize, leaveRequests.TotalPage);
        }

        public async Task<PagedVm<LeaveRequestVM>> GetPagingRequest(Expression<Func<LeaveRequests, bool>> filter, int pageSize = 10, int pageIndex = 1)
        {
            Func<IQueryable<LeaveRequests>, IOrderedQueryable<LeaveRequests>> order = x => x.OrderBy(x => x.Id);
            var leaveRequests = await this.unitOfWork.LeaveRequests
                      .GetPaging(filter: filter, orderBy: order, pageSize: pageSize, pageIndex: pageIndex);

            var leaveQuestVm = this.mapper.Map<IEnumerable<LeaveRequestVM>>(leaveRequests.Data);

            return new PagedVm<LeaveRequestVM>(leaveQuestVm, pageIndex, pageSize, leaveRequests.TotalPage);
        }
    }
}
