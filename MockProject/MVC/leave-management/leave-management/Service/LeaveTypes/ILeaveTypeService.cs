﻿using leave_management.Data;
using leave_management.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace leave_management.Service.LeaveTypes
{
    public interface ILeaveTypeService
    {
        public Task<PagedVm<DetailsLeaveTypeVM>> GetPaging(Expression<Func<LeaveType, bool>> filter, int pageSize = 10, int pageIndex = 1);
    }
}
