﻿using AutoMapper;
using leave_management.Contacts;
using leave_management.Data;
using leave_management.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace leave_management.Service.LeaveTypes
{
    public class LeaveTypeService: ILeaveTypeService
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly IMapper mapper;

        public LeaveTypeService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
        }
        public async Task<PagedVm<DetailsLeaveTypeVM>> GetPaging(Expression<Func<LeaveType, bool>> filter, int pageSize = 10, int pageIndex = 1)
        {
            Func<IQueryable<LeaveType>, IOrderedQueryable<LeaveType>> order = x => x.OrderBy(x => x.Name);
            var leaveTypes = await this.unitOfWork.LeaveTypes
                      .GetPaging(filter: filter, orderBy: order, pageSize: pageSize, pageIndex: pageIndex);

            var leaveTypeVms = this.mapper.Map<IEnumerable<DetailsLeaveTypeVM>>(leaveTypes.Data);

            return new PagedVm<DetailsLeaveTypeVM>(leaveTypeVms, pageIndex, pageSize, leaveTypes.TotalPage);
        }
    }
}
