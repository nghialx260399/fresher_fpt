#pragma checksum "D:\.Net\Fsoft\MockProject\MVC\leave-management\leave-management\Views\LeaveAllocations\Details.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "65d65e400992aaeedc71b1e7d8baa412bb0d2a0b"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_LeaveAllocations_Details), @"mvc.1.0.view", @"/Views/LeaveAllocations/Details.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "D:\.Net\Fsoft\MockProject\MVC\leave-management\leave-management\Views\_ViewImports.cshtml"
using leave_management;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "D:\.Net\Fsoft\MockProject\MVC\leave-management\leave-management\Views\_ViewImports.cshtml"
using leave_management.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"65d65e400992aaeedc71b1e7d8baa412bb0d2a0b", @"/Views/LeaveAllocations/Details.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"00e77ddfc85c045f6de54f7f6f5b70fff5cde1b1", @"/Views/_ViewImports.cshtml")]
    public class Views_LeaveAllocations_Details : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<leave_management.Models.ViewAllocationVM>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("class", new global::Microsoft.AspNetCore.Html.HtmlString("btn btn-outline-warning"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "Edit", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_2 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-controller", "LeaveAllocations", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        #pragma warning restore 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral("\r\n");
#nullable restore
#line 3 "D:\.Net\Fsoft\MockProject\MVC\leave-management\leave-management\Views\LeaveAllocations\Details.cshtml"
  
    ViewData["Title"] = "Details";

#line default
#line hidden
#nullable disable
#nullable restore
#line 6 "D:\.Net\Fsoft\MockProject\MVC\leave-management\leave-management\Views\LeaveAllocations\Details.cshtml"
 if (TempData["Info"] != null)
{

#line default
#line hidden
#nullable disable
            WriteLiteral("    <div class=\"alert alert-success alert-dismissible text-white\" role=\"alert\">\r\n        ");
#nullable restore
#line 9 "D:\.Net\Fsoft\MockProject\MVC\leave-management\leave-management\Views\LeaveAllocations\Details.cshtml"
   Write(TempData["Info"]);

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        <button type=\"button\" class=\"btn-close text-lg py-3 opacity-10\" data-bs-dismiss=\"alert\" aria-label=\"Close\">\r\n            <span aria-hidden=\"true\">&times;</span>\r\n        </button>\r\n    </div>\r\n    <br />\r\n");
#nullable restore
#line 15 "D:\.Net\Fsoft\MockProject\MVC\leave-management\leave-management\Views\LeaveAllocations\Details.cshtml"
}

#line default
#line hidden
#nullable disable
            WriteLiteral(@"<div>
    <div class=""card my-4"">
        <div class=""card-header p-0 position-relative mt-n4 mx-3 z-index-2"">
            <div class=""bg-gradient-primary shadow-primary border-radius-lg pt-4 pb-3"">
                <h6 class=""text-white text-capitalize ps-3"">Details</h6>
            </div>
        </div>
        <div class=""card-body px-0 pb-2 mr-4 ms-4"">
            <div class=""table-responsive p-0"">
                <dl class=""row m-1"">
                    <dt class=""col-sm-2"">
                        ");
#nullable restore
#line 27 "D:\.Net\Fsoft\MockProject\MVC\leave-management\leave-management\Views\LeaveAllocations\Details.cshtml"
                   Write(Html.DisplayNameFor(model => model.Employee.Username));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                    </dt>\r\n                    <dd class=\"col-sm-10\">\r\n                        ");
#nullable restore
#line 30 "D:\.Net\Fsoft\MockProject\MVC\leave-management\leave-management\Views\LeaveAllocations\Details.cshtml"
                   Write(Html.DisplayFor(model => model.Employee.Username));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                    </dd>\r\n                    <dt class=\"col-sm-2\">\r\n                        ");
#nullable restore
#line 33 "D:\.Net\Fsoft\MockProject\MVC\leave-management\leave-management\Views\LeaveAllocations\Details.cshtml"
                   Write(Html.DisplayNameFor(model => model.Employee.Email));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                    </dt>\r\n                    <dd class=\"col-sm-10\">\r\n                        ");
#nullable restore
#line 36 "D:\.Net\Fsoft\MockProject\MVC\leave-management\leave-management\Views\LeaveAllocations\Details.cshtml"
                   Write(Html.DisplayFor(model => model.Employee.Email));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                    </dd>\r\n                    <dt class=\"col-sm-2\">\r\n                        ");
#nullable restore
#line 39 "D:\.Net\Fsoft\MockProject\MVC\leave-management\leave-management\Views\LeaveAllocations\Details.cshtml"
                   Write(Html.DisplayNameFor(model => model.Employee.PhoneNumber));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                    </dt>\r\n                    <dd class=\"col-sm-10\">\r\n                        ");
#nullable restore
#line 42 "D:\.Net\Fsoft\MockProject\MVC\leave-management\leave-management\Views\LeaveAllocations\Details.cshtml"
                   Write(Html.DisplayFor(model => model.Employee.PhoneNumber));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                    </dd>\r\n                    <dt class=\"col-sm-2\">\r\n                        ");
#nullable restore
#line 45 "D:\.Net\Fsoft\MockProject\MVC\leave-management\leave-management\Views\LeaveAllocations\Details.cshtml"
                   Write(Html.DisplayNameFor(model => model.Employee.FirstName));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                    </dt>\r\n                    <dd class=\"col-sm-10\">\r\n                        ");
#nullable restore
#line 48 "D:\.Net\Fsoft\MockProject\MVC\leave-management\leave-management\Views\LeaveAllocations\Details.cshtml"
                   Write(Html.DisplayFor(model => model.Employee.FirstName));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                    </dd>\r\n                    <dt class=\"col-sm-2\">\r\n                        ");
#nullable restore
#line 51 "D:\.Net\Fsoft\MockProject\MVC\leave-management\leave-management\Views\LeaveAllocations\Details.cshtml"
                   Write(Html.DisplayNameFor(model => model.Employee.LastName));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                    </dt>\r\n                    <dd class=\"col-sm-10\">\r\n                        ");
#nullable restore
#line 54 "D:\.Net\Fsoft\MockProject\MVC\leave-management\leave-management\Views\LeaveAllocations\Details.cshtml"
                   Write(Html.DisplayFor(model => model.Employee.LastName));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                    </dd>\r\n                    <dt class=\"col-sm-2\">\r\n                        ");
#nullable restore
#line 57 "D:\.Net\Fsoft\MockProject\MVC\leave-management\leave-management\Views\LeaveAllocations\Details.cshtml"
                   Write(Html.DisplayNameFor(model => model.Employee.TaxId));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                    </dt>\r\n                    <dd class=\"col-sm-10\">\r\n                        ");
#nullable restore
#line 60 "D:\.Net\Fsoft\MockProject\MVC\leave-management\leave-management\Views\LeaveAllocations\Details.cshtml"
                   Write(Html.DisplayFor(model => model.Employee.TaxId));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                    </dd>\r\n                    <dt class=\"col-sm-2\">\r\n                        ");
#nullable restore
#line 63 "D:\.Net\Fsoft\MockProject\MVC\leave-management\leave-management\Views\LeaveAllocations\Details.cshtml"
                   Write(Html.DisplayNameFor(model => model.Employee.DateOfBirth));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                    </dt>\r\n                    <dd class=\"col-sm-10\">\r\n                        ");
#nullable restore
#line 66 "D:\.Net\Fsoft\MockProject\MVC\leave-management\leave-management\Views\LeaveAllocations\Details.cshtml"
                   Write(Html.DisplayFor(model => model.Employee.DateOfBirth));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                    </dd>\r\n                    <dt class=\"col-sm-2\">\r\n                        ");
#nullable restore
#line 69 "D:\.Net\Fsoft\MockProject\MVC\leave-management\leave-management\Views\LeaveAllocations\Details.cshtml"
                   Write(Html.DisplayNameFor(model => model.Employee.DateJoined));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                    </dt>\r\n                    <dd class=\"col-sm-10\">\r\n                        ");
#nullable restore
#line 72 "D:\.Net\Fsoft\MockProject\MVC\leave-management\leave-management\Views\LeaveAllocations\Details.cshtml"
                   Write(Html.DisplayFor(model => model.Employee.DateJoined));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                    </dd>\r\n                    <dt class=\"col-sm-2\">\r\n                        ");
#nullable restore
#line 75 "D:\.Net\Fsoft\MockProject\MVC\leave-management\leave-management\Views\LeaveAllocations\Details.cshtml"
                   Write(Html.DisplayNameFor(model => model.Employee.DateCreated));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                    </dt>\r\n                    <dd class=\"col-sm-10\">\r\n                        ");
#nullable restore
#line 78 "D:\.Net\Fsoft\MockProject\MVC\leave-management\leave-management\Views\LeaveAllocations\Details.cshtml"
                   Write(Html.DisplayFor(model => model.Employee.DateCreated));

#line default
#line hidden
#nullable disable
            WriteLiteral(@"
                    </dd>
                </dl>
            </div>
        </div>
    </div>
</div>

<br />
<div class=""card my-4"">
    <div class=""card-header p-0 position-relative mt-n4 mx-3 z-index-2"">
        <div class=""bg-gradient-primary shadow-primary border-radius-lg pt-4 pb-3"">
            <h6 class=""text-white text-capitalize ps-3"">Leave Allocations</h6>
        </div>
    </div>
    <div class=""card-body px-0 pb-2 mr-4 ms-4"">
        <div class=""table-responsive p-0"">
            <ul class=""list-group list-group-flush"">
");
#nullable restore
#line 96 "D:\.Net\Fsoft\MockProject\MVC\leave-management\leave-management\Views\LeaveAllocations\Details.cshtml"
                 foreach (var item in Model.LeaveAllocations)
                {

#line default
#line hidden
#nullable disable
            WriteLiteral("                    <li class=\"list-group-item\">\r\n                        <h6>");
#nullable restore
#line 99 "D:\.Net\Fsoft\MockProject\MVC\leave-management\leave-management\Views\LeaveAllocations\Details.cshtml"
                       Write(item.LeaveType.Name);

#line default
#line hidden
#nullable disable
            WriteLiteral(" <span class=\"badge badge-secondary\">");
#nullable restore
#line 99 "D:\.Net\Fsoft\MockProject\MVC\leave-management\leave-management\Views\LeaveAllocations\Details.cshtml"
                                                                                Write(item.NumberOfDays);

#line default
#line hidden
#nullable disable
            WriteLiteral("</span></h6>\r\n                        ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "65d65e400992aaeedc71b1e7d8baa412bb0d2a0b14677", async() => {
                WriteLiteral("\r\n                            Edit\r\n                        ");
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = (string)__tagHelperAttribute_1.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_1);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Controller = (string)__tagHelperAttribute_2.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_2);
            if (__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues == null)
            {
                throw new InvalidOperationException(InvalidTagHelperIndexerAssignment("asp-route-id", "Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper", "RouteValues"));
            }
            BeginWriteTagHelperAttribute();
#nullable restore
#line 100 "D:\.Net\Fsoft\MockProject\MVC\leave-management\leave-management\Views\LeaveAllocations\Details.cshtml"
                                                                                                                 WriteLiteral(item.Id);

#line default
#line hidden
#nullable disable
            __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"] = __tagHelperStringValueBuffer;
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-route-id", __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"], global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n                    </li>\r\n");
#nullable restore
#line 104 "D:\.Net\Fsoft\MockProject\MVC\leave-management\leave-management\Views\LeaveAllocations\Details.cshtml"
                }

#line default
#line hidden
#nullable disable
            WriteLiteral("            </ul>\r\n        </div>\r\n    </div>\r\n</div>\r\n\r\n");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<leave_management.Models.ViewAllocationVM> Html { get; private set; }
    }
}
#pragma warning restore 1591
