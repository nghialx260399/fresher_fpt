﻿using Phone_Accessories.ViewModels.Carts;
using Phone_Accessories.ViewModels.Products;

namespace Phone_Accessories.Application.Carts
{
    public interface ICartService
    {
        Task<IList<ViewProduct>> GetCart(string userId);

        Task<bool> Create(CartVm createCart);

        Task<bool> Update(CartVm createCart);

        Task<bool> Delete(string userId, int productId);
    }
}