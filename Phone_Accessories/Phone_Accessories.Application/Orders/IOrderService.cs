﻿using Phone_Accessories.ViewModels.Orders;

namespace Phone_Accessories.Application.Orders
{
    public interface IOrderService
    {
        Task<IList<ViewOrderBasic>> GetAll();

        Task<IList<ViewOrderBasic>> GetByUserId(string userId);

        Task<bool> Create(CreateOrder createOrder);

        Task<bool> Update(int id);

        Task<bool> Reject(int id);

        Task<ViewOrder> GetById(int id);
    }
}