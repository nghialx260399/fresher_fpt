﻿using Phone_Accessories.ViewModels.Products;

namespace Phone_Accessories.Application.Products
{
    public interface IProductService
    {
        Task<IList<ViewProduct>> GetAll();

        Task<IList<ViewProduct>> GetQuantityWarning();

        Task<IList<ViewProduct>> GetBestSold();

        Task<IList<ViewProduct>> GetLast();

        //Task<IList<Statistical>> Statistical();

        Task<IList<ViewProduct>> GetPromotioPriceBest();

        Task<IList<ViewProduct>> GetAllDistinct();

        Task<bool> Create(CreateProduct createProduct);

        Task<ViewProduct> GetById(int id);

        Task<ViewProduct> GetBySize(int sizeId, string productName, int colorId);

        Task<ViewProduct> GetByColor(int colorId, string productName, int sizeId);

        Task<bool> Update(ViewProduct createProduct);

        Task<bool> Delete(int id);

        Task<IList<ViewProduct>> Search(string name);

        Task<IList<ViewProduct>> GetByCategory(int categoryId);
    }
}