﻿using Phone_Accessories.Data.Infastructures;
using Phone_Accessories.Models.Entity;

namespace Phone_Accessories.Data.IRepositories
{
    public interface ICartRepository : IGenericRepository<Cart>
    {
        public Task<Cart> GetCart(string userId);
    }
}