﻿using Phone_Accessories.Data.Infastructures;
using Phone_Accessories.Models.Entity;

namespace Phone_Accessories.Data.IRepositories
{
    public interface IProductRepository : IGenericRepository<Product>
    {
        public Task<Product> GetByIdInclude(int id);
    }
}