﻿using Phone_Accessories.Data.IRepositories;

namespace Phone_Accessories.Data.Infastructures
{
    public interface IUnitOfWork : IDisposable
    {
        ICategoryRepository CategoryRepository { get; }

        IProductRepository ProductRepository { get; }

        IOrderRepository OrderRepository { get; }

        IOrderDetailRepository OrderDetailRepository { get; }

        ICartRepository CartRepository { get; }

        ICartDetailRepository CartDetailRepository { get; }

        ISizeRepository SizeRepository { get; }

        IColorReporitory ColorRepository { get; }

        ApplicationDbContext ApplicationDbContext { get; }

        Task<int> SaveChanges();
    }
}