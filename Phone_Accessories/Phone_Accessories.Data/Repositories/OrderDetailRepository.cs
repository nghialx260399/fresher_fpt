﻿using Microsoft.EntityFrameworkCore;
using Phone_Accessories.Data.Infastructures;
using Phone_Accessories.Data.IRepositories;
using Phone_Accessories.Models.Entity;
using System.Linq.Expressions;

namespace Phone_Accessories.Data.Repositories
{
    public class OrderDetailRepository : GenericRepository<OrderDetail>, IOrderDetailRepository
    {
        public OrderDetailRepository(ApplicationDbContext context) : base(context)
        {
        }

        public async Task<IList<OrderDetail>> GetAllInclude(Expression<Func<OrderDetail, bool>> condition)
        {
            return await this.DbSet.Where(condition).Include(x => x.Product).ToListAsync();
        }
    }
}