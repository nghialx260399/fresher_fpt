﻿using Phone_Accessories.Models.BaseEntity;

namespace Phone_Accessories.Models.Entity
{
    public class CartDetail : EntityBase
    {
        public int CartId { get; set; }

        public Cart? Cart { get; set; }

        public int ProductId { get; set; }

        public Product? Product { get; set; }

        public int Quantity { get; set; }
    }
}