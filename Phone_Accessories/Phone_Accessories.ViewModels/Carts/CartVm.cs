﻿namespace Phone_Accessories.ViewModels.Carts
{
    public class CartVm
    {
        public string? UserId { get; set; }

        public int ProductId { get; set; }

        public int Quantity { get; set; }
    }
}