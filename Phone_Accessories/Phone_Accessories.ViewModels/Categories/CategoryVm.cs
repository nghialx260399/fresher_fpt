﻿using Phone_Accessories.ViewModels.EntityBaseVms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phone_Accessories.ViewModels.Categories
{
    public class CategoryVm : EntityBaseVm
    {
        public int Id { get; set; }

        public string? Name { get; set; }
    }
}
