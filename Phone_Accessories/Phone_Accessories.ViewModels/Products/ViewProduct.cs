﻿using Phone_Accessories.ViewModels.Categories;
using Phone_Accessories.ViewModels.Colors;
using Phone_Accessories.ViewModels.EntityBaseVms;
using Phone_Accessories.ViewModels.Sizes;

namespace Phone_Accessories.ViewModels.Products
{
    public class ViewProduct : EntityBaseVm
    {
        public int Id { get; set; }

        public string? Name { get; set; }

        public string? Description { get; set; }

        public byte[]? Image { get; set; }

        public int ColorId { get; set; }

        public ColorVm? Color { get; set; }

        public int SizeId { get; set; }

        public SizeVm? Size { get; set; }

        public int Quantity { get; set; }

        public int Sold { get; set; }

        public decimal Price { get; set; }

        public int PromotionPrice { get; set; }

        public int CategoryId { get; set; }

        public CategoryVm? Category { get; set; }
    }
}