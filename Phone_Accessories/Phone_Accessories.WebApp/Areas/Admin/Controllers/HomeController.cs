﻿using Microsoft.AspNetCore.Mvc;

namespace Phone_Accessories.WebApp.Areas.Admin.Controllers
{
    public class HomeController : BaseAdminController
    {
        public IActionResult Index()
        {
            return View();
        }
    }
}
