﻿using Microsoft.AspNetCore.Mvc;
using Phone_Accessories.Application.Orders;

namespace Phone_Accessories.WebApp.Areas.Admin.Controllers
{
    public class OrderController : BaseAdminController
    {
        private readonly IOrderService orderService;

        public OrderController(IOrderService orderService)
        {
            this.orderService = orderService;
        }

        public async Task<IActionResult> GetAll()
        {
            return View(await orderService.GetAll());
        }

        public async Task<IActionResult> GetById(int id)
        {
            var order = await orderService.GetById(id);
            if (order != null)
                return View(order);

            return BadRequest();
        }

        [HttpGet]
        public async Task<IActionResult> Update(int id)
        {
            if (!ModelState.IsValid)
                return BadRequest("Invalid Data");

            bool isSuccess = await orderService.Update(id);

            if (isSuccess)
                return RedirectToAction("GetAll");

            return BadRequest();
        }

        [HttpGet]
        public async Task<IActionResult> Reject(int id)
        {
            if (!ModelState.IsValid)
                return BadRequest("Invalid Data");

            bool isSuccess = await orderService.Reject(id);

            if (isSuccess)
                return RedirectToAction("GetAll");

            return BadRequest();
        }
    }
}
