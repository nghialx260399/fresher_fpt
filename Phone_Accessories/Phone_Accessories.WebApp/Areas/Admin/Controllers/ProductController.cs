﻿using Microsoft.AspNetCore.Mvc;
using Phone_Accessories.Application.Products;
using Phone_Accessories.ViewModels.Products;

namespace Phone_Accessories.WebApp.Areas.Admin.Controllers
{
    public class ProductController : BaseAdminController
    {
        private readonly ILogger<CategoryController> logger;
        private readonly IProductService categoryService;

        public ProductController(ILogger<CategoryController> logger, IProductService categoryService)
        {
            this.logger = logger;
            this.categoryService = categoryService;
        }
        public async Task<IActionResult> Index()
        {
            var categories = await categoryService.GetAll();

            return View(categories);
        }

        public async Task<IActionResult> Create()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(CreateProduct categoryVm)
        {
            if (!ModelState.IsValid)
            {
                TempData["InforCreate"] = "Create fail";
                return View(categoryVm);
            }

            var files = HttpContext.Request.Form.Files;
            if (files.Count > 0)
            {
                byte[] p1 = null;
                using (var fs1 = files[0].OpenReadStream())
                {
                    using (var ms1 = new MemoryStream())
                    {
                        fs1.CopyTo(ms1);
                        p1 = ms1.ToArray();
                    }
                }

                categoryVm.Image = p1;
            }
        

            var isSuccess = await categoryService.Create(categoryVm);

            if (isSuccess)
            {
                TempData["InforCreate"] = "Create success";
                return RedirectToAction("Index", "Product");
            }
            return View(categoryVm);
        }

        public async Task<IActionResult> Edit(int id)
        {
            var categoryVm = await categoryService.GetById(id);

            if (categoryVm != null)
            {
                return View(categoryVm);
            }
                
            return RedirectToAction("Index", "Product");
        }

        [HttpPost]
        public async Task<IActionResult> Edit(ViewProduct categoryVm)
        {
            if (!ModelState.IsValid)
            {
                TempData["InforUpdate"] = "Update fail";
                return View(categoryVm);
            }

            var files = HttpContext.Request.Form.Files;
            if (files.Count > 0)
            {
                byte[] p1 = null;
                using (var fs1 = files[0].OpenReadStream())
                {
                    using (var ms1 = new MemoryStream())
                    {
                        fs1.CopyTo(ms1);
                        p1 = ms1.ToArray();
                    }
                }

                categoryVm.Image = p1;
            }
            //else
            //{
            //    var objFormDb = await categoryService.GetById(categoryVm.Id);
            //    categoryVm.Image = (byte[]?)TempData["Image"];
            //}

            var isSuccess = await categoryService.Update(categoryVm);

            if (isSuccess)
            {
                TempData["InforUpdate"] = "Update success";
                return RedirectToAction("Index", "Product");
            }
            return View(categoryVm);
        }

        public async Task<IActionResult> Delete(int id)
        {
            var categoryVm = await categoryService.GetById(id);

            if (categoryVm != null)
                return View(categoryVm);

            return RedirectToAction("Index", "Product");
        }

        [HttpPost]
        public async Task<IActionResult> Delete(int id, IFormCollection collection)
        {
            if (!ModelState.IsValid)
                TempData["InforDelete"] = "Delete fail";

            var isSuccess = await categoryService.Delete(id);

            if (isSuccess)
            {
                TempData["InforDelete"] = "Delete success";
                return RedirectToAction("Index", "Product");
            }

            return View();
        }
    }
}
