﻿using Microsoft.AspNetCore.Mvc;
using Phone_Accessories.Application.Carts;
using Phone_Accessories.ViewModels.Carts;

namespace Phone_Accessories.WebApp.Controllers
{
    public class CartController : Controller
    {
        private readonly ICartService cartService;

        public CartController(ICartService cartService)
        {
            this.cartService = cartService;
        }

        [HttpGet]
        public async Task<IActionResult> Index(string userId)
        {
            var carts = await cartService.GetCart(userId);
            if (carts == null)
                return RedirectToAction("Index", "Home");

            return View(carts);
        }

        [HttpPost]
        public async Task<bool> Create(string userId, int productId, int quantity)
        {
            var createCart = new CartVm()
            {
                UserId = userId,
                ProductId = productId,
                Quantity = quantity
            };
            bool isSuccess = await cartService.Create(createCart);

            if (isSuccess)
                return true;

            return false;
        }

        public async Task<IActionResult> Delete(string userId, int productId)
        {
            if (!ModelState.IsValid)
                return BadRequest("Invalid Data");

            bool isSuccess = await cartService.Delete(userId, productId);

            if (isSuccess)
                return RedirectToAction("Index", "Cart", new {userId = userId});

            return BadRequest();
        }
    }
}
