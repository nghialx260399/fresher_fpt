﻿using Microsoft.AspNetCore.Mvc;
using Phone_Accessories.Application.Products;

namespace Phone_Accessories.WebApp.Controllers
{
    public class ProductController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IProductService productService;

        public ProductController(ILogger<HomeController> logger, IProductService productService)
        {
            _logger = logger;
            this.productService = productService;
        }

        public async Task<IActionResult> GetProductBycategory(int id)
        {
            var categories = await productService.GetByCategory(id);

            return PartialView("_Product", categories);
        }

        public async Task<IActionResult> ProductDetail(int id)
        {
            var categories = await productService.GetById(id);
            return PartialView("_ProductDetail", categories);
        }

        public async Task<IActionResult> GetBySize(int sizeId, string name, int colorId, int id)
        {
            var categories = await productService.GetBySize(sizeId, name, colorId);
            if (categories.Id != 0)
                return PartialView("_ProductDetail", categories);
            else return RedirectToAction("ProductDetail", new { id = id });
        }

        public async Task<IActionResult> GetByColor(int sizeId, string name, int colorId, int id)
        {
            var categories = await productService.GetByColor(colorId, name, sizeId);
            if (categories.Id != 0)
                return PartialView("_ProductDetail", categories);
            else return RedirectToAction("ProductDetail", new { id = id });
        }
    }
}
